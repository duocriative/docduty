import React from 'react'
import { Provider as PaperProvider } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'
import AuthStackNavigator from './src/navigation/AuthStackNavigator'

import { theme } from './src/core/theme'


export default function App() {
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer>
        <AuthStackNavigator />
      </NavigationContainer>
    </PaperProvider>
  )
}
