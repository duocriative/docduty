import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Card, Title, Paragraph } from 'react-native-paper'
import { useNavigation } from '@react-navigation/native'

export default function CardConsulta({ title, text, button, cor, onPress }) {
  const navigation = useNavigation()
  return (
    <Card style={{ paddingLeft: 14, backgroundColor: cor, borderRadius: 14, marginVertical: 11, }}>
      <Card.Content style={styles.cardContent}>
        <View style={styles.cardTextContent}>
          <Title style={styles.cardTitle} onPress={onPress}>{title}</Title>
          {text && <Paragraph>{text}</Paragraph>}
        </View>
        {button && (
          <TouchableOpacity
            style={{ width: 125, height: 27, borderRadius: 27, backgroundColor: cor, display: 'flex', alignItems: 'center', justifyContent: 'center', }}
            onPress={onPress}
          >
            <Text style={styles.buttonText}>{button}</Text>
          </TouchableOpacity>
        )}
      </Card.Content>
    </Card>
  )
}

const styles = StyleSheet.create({
  card: {
    paddingLeft: 14,
    backgroundColor: '#fd7e02',
    borderRadius: 14,
    marginVertical: 11,
  },
  cardTextContent: {
    maxWidth: '50%',
  },
  cardTitle: {
    fontWeight: 'bold',
  },
  cardContent: {
    borderTopEndRadius: 14,
    borderBottomEndRadius: 14,
    paddingLeft: 10,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: 106,
  },
  button: {
    width: 125,
    height: 27,
    borderRadius: 27,
    backgroundColor: '#fd7e02',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 14,
    color: '#fff',
  },
})
