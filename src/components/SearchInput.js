import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { Searchbar } from 'react-native-paper'
import { theme } from '../core/theme'

export default function SearchInput({ errorText, description, style, ...props }) {
  return (
    <View style={styles.container}>
      {props.labelOut ? (
        <Text style={(styles.label, style)}>{props.labelOut}</Text>
      ) : null}
      <Searchbar
        style={styles.input}
        selectionColor={theme.colors.primary}
        underlineColor="transparent"
        mode="outlined"
        {...props}
        />
      {description && !errorText ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 24,
  },
  input: {
    backgroundColor: theme.colors.surface,
    shadowColor: 'white'
  },
  label: {
    fontSize: 15,
    fontWeight: 'bold',
    color: theme.colors.text,
  },
  description: {
    fontSize: 13,
    color: theme.colors.secondary,
    paddingTop: 8,
  },
  error: {
    fontSize: 13,
    color: theme.colors.error,
    paddingTop: 8,
  },
})