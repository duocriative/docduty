import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Card, Title } from 'react-native-paper'
import MetaInfoIcon from './MetaInfoIcon'

export default function CardConsulta({
  title,
  hora,
  dia,
  cor,
  status,
  button,
  onPress,
}) {
  return (
    <Card style={{ paddingLeft: 14, backgroundColor: cor, borderRadius: 14, marginVertical: 11, }}>
      <Card.Content style={styles.cardContent}>
        <View style={styles.cardTextContent}>
          <Title onPress={onPress} style={styles.cardTitle}>
            {title}
          </Title>
          <View style={styles.cardMetaContent}>
            <MetaInfoIcon text={dia} icon="CalendarMonthOutline" />
            <MetaInfoIcon text={hora} icon="AvTimer" />
          </View>
        </View>
        <View style={styles.cardTagsContet}>
          {button && (
            <TouchableOpacity style={{ width: 120, height: 27, marginBottom: 10, borderRadius: 27, backgroundColor: cor, display: 'flex', alignItems: 'center', justifyContent: 'center', }} onPress={onPress}>
              <Text style={styles.buttonText}>{button}</Text>
            </TouchableOpacity>
          )}
          {status && <Text style={styles.status}>{status}</Text>}
        </View>
      </Card.Content>
    </Card>
  )
}

const styles = StyleSheet.create({
  card: {
    paddingLeft: 14,
    backgroundColor: '#fd7e02',
    borderRadius: 14,
    marginVertical: 11,
  },
  cardTextContent: {
    width: '55%',
  },
  cardTitle: {
    fontWeight: 'bold',
    fontSize: 18
  },
  cardContent: {
    borderTopEndRadius: 14,
    borderBottomEndRadius: 14,
    paddingLeft: 10,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: 106,
  },
  button: {
    width: 125,
    height: 27,
    marginBottom: 10,
    borderRadius: 27,
    backgroundColor: '#fd7e02',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 12,
    color: '#fff',
  },
  metaInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  metaInfoText: {
    marginLeft: 5,
    fontSize: 18,
    color: '#000',
  },
  metaInfoIcon: {
    fontSize: 18,
    color: '#000',
    marginRight: 30,
  },
  cardMetaContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  status: {
    fontSize: 12,
  },
  cardTagsContet: {
    alignItems: 'center',
  },
})
