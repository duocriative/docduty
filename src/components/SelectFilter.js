import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import SectionedMultiSelect from 'react-native-sectioned-multi-select'
import { theme } from '../core/theme'

export default function SelectFilter({ errorText, description, ...props }) {
  return (
    <View style={styles.container}>
      {props.labelOut ? (
        <Text style={styles.label}>{props.labelOut}</Text>
      ) : null}
      <SectionedMultiSelect
        styles={stylesSelect}
        IconRenderer={MaterialIcons}
        uniqueKey="id"
        subKey="children"
        {...props}
      />
      {description && !errorText ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  )
}
const stylesSelect = {
  selectToggle: {
    backgroundColor: '#fff',
    borderColor: '#999',
    borderWidth: 1,
    borderRadius: 2,
    padding: 16,
    marginTop: 6,
  },
  // chipText: {
  //   maxWidth: Dimensions.get('screen').width - 90,
  // },
  // itemText: {
  //   color: this.state.selectedItems.length ? 'black' : 'lightgrey'
  // },
  selectedItemText: {
    color: 'blue',
  },
  // subItemText: {
  //   color: this.state.selectedItems.length ? 'black' : 'lightgrey'
  // },
  selectedSubItemText: {
    color: 'blue',
  },
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 12,
  },
  label: {
    fontSize: 15,
    //fontWeight: 'bold',
    color: theme.colors.text,
  },
  description: {
    fontSize: 13,
    color: theme.colors.secondary,
    paddingTop: 8,
  },
  error: {
    fontSize: 13,
    color: theme.colors.error,
    paddingTop: 8,
  },
})
