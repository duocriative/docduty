import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import TagInput from 'react-native-tags-input'
import { theme } from '../core/theme'

export default function TagsInput({
  errorText,
  description,
  style,
  ...props
}) {
  return (
    <View style={styles.container}>
      {props.labelOut ? (
        <Text style={(styles.label, style)}>{props.labelOut}</Text>
      ) : null}
      <TagInput
          updateState={this.updateTagState}
          tags={this.state.tags}
          {...props}
          />
      {description && !errorText ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 12,
  },
  label: {
    fontSize: 15,
    fontWeight: 'bold',
    color: theme.colors.text,
  },
  description: {
    fontSize: 13,
    color: theme.colors.secondary,
    paddingTop: 8,
  },
  error: {
    fontSize: 13,
    color: theme.colors.error,
    paddingTop: 8,
  },
})
