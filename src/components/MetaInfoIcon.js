import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {
  CalendarMonthOutline,
  AvTimer,
  AccountOutline,
  MapMarkerOutline,
  Whatsapp,
  Stethoscope,
} from '../assets/icons'

export default function MetaInfoIcon({ icon, text, style, fillIcon }) {
  const styleIcon =
    style && style.icon
      ? (styles.metaInfoIcon, style.icon)
      : styles.metaInfoIcon
  const fill = fillIcon || styles.metaInfoIcon.color
  const styleText =
    style && style.text
      ? (styles.metaInfoText, style.text)
      : styles.metaInfoText

  return (
    <View style={styles.metaInfo}>
      {icon && icon === 'CalendarMonthOutline' && (
        <CalendarMonthOutline
          style={styleIcon}
          fill={styles.metaInfoIcon.color}
        />
      )}
      {icon && icon === 'AvTimer' && <AvTimer style={styleIcon} fill={fill} />}
      {icon && icon === 'AccountOutline' && (
        <AccountOutline style={styleIcon} fill={fill} />
      )}
      {icon && icon === 'MapMarkerOutline' && (
        <MapMarkerOutline style={styleIcon} fill={fill} />
      )}
      {icon && icon === 'Whatsapp' && (
        <Whatsapp style={styleIcon} fill={fill} />
      )}
      {icon && icon === 'Stethoscope' && (
        <Stethoscope style={styleIcon} fill={fill} />
      )}
      <Text style={styleText}>{text}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  metaInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },
  metaInfoText: {
    marginLeft: 5,
    fontSize: 18,
    color: '#000',
  },
  metaInfoIcon: {
    fontSize: 18,
    color: '#000',
  },
})
