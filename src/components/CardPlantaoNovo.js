import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Card, Title } from 'react-native-paper'
import MetaInfoIcon from './MetaInfoIcon'

export default function CardPlantaoNovo({
  title,
  tipo,
  hora,
  dia,
  local,
  anunciante,
  cor,
  sexo,
  button,
  onPress,
}) {
  return (
    <Card style={{ paddingLeft: 14, backgroundColor: cor, borderRadius: 14, marginVertical: 11 }}>
      <Card.Content style={styles.cardContent}>
        <View style={styles.cardHead}>
          <Title style={styles.cardTitle} onPress={onPress}>
            {title} {tipo}
          </Title>
          {button && (
            <TouchableOpacity style={{ backgroundColor: cor, width: 120, height: 27, marginBottom: 10, borderRadius: 27, display: 'flex', alignItems: 'center', justifyContent: 'center' }} onPress={onPress}>
              <Text style={styles.buttonText}>{button}</Text>
            </TouchableOpacity>
          )}
        </View>
        <View style={styles.cardMetaContent}>
          <MetaInfoIcon text={local} icon="MapMarkerOutline" />
        </View>
        <View style={styles.cardMetaContent}>
          <MetaInfoIcon text={dia} icon="CalendarMonthOutline" />
          <MetaInfoIcon text={hora} icon="AvTimer" />
        </View>
        <View style={styles.cardMetaContent}>
          <MetaInfoIcon text={'Dr. ' + anunciante} icon="Stethoscope" />
        </View>
      </Card.Content>
    </Card>
  )
}

const styles = StyleSheet.create({
  card: {
    paddingLeft: 14,
    backgroundColor: '#fd7e02',
    borderRadius: 14,
    marginVertical: 11,
  },
  cardHead: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#b6b4b4',
    paddingVertical: 10,
    marginBottom: 10,
  },
  cardTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 23,
    textTransform: 'capitalize',
    width: '50%'
  },
  cardContent: {
    borderTopEndRadius: 14,
    borderBottomEndRadius: 14,
    paddingLeft: 10,
    backgroundColor: '#fff',
    minHeight: 106,
  },
  button: {
    width: 125,
    height: 27,
    marginBottom: 10,
    borderRadius: 27,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 12,
    color: '#fff',
  },
  metaInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  metaInfoText: {
    marginLeft: 5,
    fontSize: 18,
    color: '#000',
  },
  metaInfoIcon: {
    fontSize: 18,
    color: '#000',
    marginRight: 30,
  },
  cardMetaContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 10,
  },
  cardTagsContet: {
    alignItems: 'center',
  },
})
