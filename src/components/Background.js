import React from 'react'
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView,
} from 'react-native'
import { theme } from '../core/theme'

export default function Background({ children }) {
  return (
    <View style={styles.background}>
      <SafeAreaView>
        <KeyboardAvoidingView behavior="padding">
          <ScrollView>
            <View style={styles.container}>{children}</View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  )
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    backgroundColor: theme.colors.tint,
  },
  container: {
    padding: 20,
    width: '100%',
  },
})
