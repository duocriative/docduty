import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import Logo from './Logo'
import { getUserData } from '../api/auth-api'

export default function TopBar() {
  const user = getUserData()
  const avatarSource =
    user && user.userType === 'medico'
      ? require('../assets/avatarMedico.png')
      : require('../assets/avatar.png')

  return (
    <View style={styles.container}>
      <Logo size="mini" />
      <View style={styles.avatar}>
        <Image source={avatarSource} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingTop: 0,
    paddingBottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  avatar: {
    width: 60,
    height: 60,
    backgroundColor: '#fff',
    borderWidth: 3,
    borderColor: '#00154c',
    borderRadius: 60,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
