import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { Picker } from '@react-native-picker/picker'
import { theme } from '../core/theme'

export default ({ errorText, description, ...props }) => (
  <View style={styles.container}>
    <Picker
      style={styles.pickerStyleType}
      selectionColor={theme.colors.primary}
      underlineColor="transparent"
      mode="outlined"
      selectedValue={props.value}
      onValueChange={props.onValueChange}
    >
      {props.data.map((estado) => (
        <Picker.Item key={estado} label={estado.sigla} value={estado} />
      ))}
    </Picker>
    {description && !errorText ? (
      <Text style={styles.description}>{description}</Text>
    ) : null}
    {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
  </View>
)

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 12,
  },
  pickerStyleType: {
    backgroundColor: theme.colors.surface,
  },
  description: {
    fontSize: 13,
    color: theme.colors.secondary,
    paddingTop: 8,
  },
  error: {
    fontSize: 13,
    color: theme.colors.error,
    paddingTop: 8,
  },
})
