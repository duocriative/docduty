import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'
import { Card, Title } from 'react-native-paper'
import MetaInfoIcon from './MetaInfoIcon'
import Button from './Button'

export default function CardMedico({
  nome,
  crm,
  telefone,
  anunciante,
  button,
  onPress,
}) {

  return (
    <Card style={styles.card}>
      <Card.Content style={styles.cardContent}>
        <View style={styles.avatar}>
          <Image source={require('../assets/avatarMedico.png')} />
        </View>
        <View style={styles.cardText}>
          <Title style={styles.cardTitle} onPress={onPress}>
            {'Dr. ' + nome}
          </Title>
          <View style={styles.cardMetaContent}>
            <Text>CRM: {crm}</Text>
            <MetaInfoIcon
              text="(21) 9999-9999"
              icon="Whatsapp"
              fillIcon="#117d06"
            />
          </View>
        </View>
      </Card.Content>
    </Card>
  )
}

const styles = StyleSheet.create({
  card: {
    paddingLeft: 14,
    backgroundColor: '#fd7e02',
    borderRadius: 14,
    marginVertical: 11,
  },
  cardHead: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#b6b4b4',
    paddingVertical: 10,
    marginBottom: 10,
  },
  cardTitle: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
  cardContent: {
    borderTopEndRadius: 14,
    borderBottomEndRadius: 14,
    paddingLeft: 10,
    backgroundColor: '#fff',
    minHeight: 106,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  button: {
    width: 125,
    height: 27,
    marginBottom: 10,
    borderRadius: 27,
    backgroundColor: '#fd7e02',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 14,
    color: '#fff',
  },
  metaInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  metaInfoText: {
    marginLeft: 5,
    fontSize: 18,
    color: '#000',
  },
  metaInfoIcon: {
    fontSize: 18,
    color: '#000',
    marginRight: 30,
  },
  cardMetaContent: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: 10,
  },
  cardTagsContet: {
    alignItems: 'center',
  },
  avatar: {
    width: 60,
    height: 60,
    backgroundColor: '#fff',
    borderWidth: 3,
    marginRight: 15,
    borderColor: '#00154c',
    borderRadius: 60,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }
})
