import React from 'react'
import { Image, StyleSheet } from 'react-native'

export default function Logo({ size }) {
  const styleSize = size === 'mini' ? styles.mini : styles.default
  const logoUrl =
    size === 'mini'
      ? require('../assets/logomini.png')
      : require('../assets/logo.png')

  return <Image source={logoUrl} style={styleSize} />
}

const styles = StyleSheet.create({
  default: {
    width: 298,
    height: 216,
  },
  mini: {
    width: 107,
    height: 77,
  },
})
