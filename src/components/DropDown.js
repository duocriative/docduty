import React, { useState } from 'react'
import { View, StyleSheet, Text } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker'
import { theme } from '../core/theme'

export default function DropDown({ errorText, description, style, ...props }) {
  const [open, setOpen] = useState(false)
  const [value, setValue] = useState(props.valor)
  const [items, setItems] = useState(props.itens)

  return (
    <View style={styles.container}>
      {props.labelOut ? (
        <Text style={(styles.label, style)}>{props.labelOut}</Text>
      ) : null}
      <DropDownPicker
        open={open}
        value={value}
        items={items}
        setOpen={setOpen}
        setValue={setValue}
        setItems={setItems}
        listMode="MODAL"
        closeAfterSelecting
        {...props}
      />
      {description && !errorText ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 12,
  },
  label: {
    fontSize: 15,
    fontWeight: 'bold',
    color: theme.colors.text,
  },
  description: {
    fontSize: 13,
    color: theme.colors.secondary,
    paddingTop: 8,
  },
  error: {
    fontSize: 13,
    color: theme.colors.error,
    paddingTop: 8,
  },
})
