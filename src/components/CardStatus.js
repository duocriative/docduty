import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Card, Title, Paragraph } from 'react-native-paper'
import statusPedidos from '../helpers/statusPedidos.json'

export default function CardStatus({ title, text, status }) {

    return (
        <View style={styles.cardStatus}>
            <View>
                <Title style={styles.cardStatusTitle}>
                    {statusPedidos[status].title}
                </Title>
                <View>
                    <Text style={styles.cardStatusContent}>
                        {statusPedidos[status].message}
                    </Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
  cardStatus: {
      backgroundColor: '#000d2f',
      marginTop: 16,
      padding: 24,
      textAlign: 'center'
  },
  cardStatusTitle: {
      color: '#43c0ba',
      marginBottom: 12,
      textAlign: 'center'
  },
  cardStatusContent: {
    color: '#ffffff',
    fontSize: 14,
    textAlign: 'center'
  },
})