import React, { useState } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { Text } from 'react-native-paper'
import Background from '../components/Background'
import Header from '../components/Header'
import Button from '../components/Button'
import TextInput from '../components/TextInput'
import { estados } from '../helpers/estados-cidades.json'
import { convenios } from '../helpers/convenios.json'
import SelectFilter from '../components/SelectFilter'
import { theme } from '../core/theme'

import { signUpUser } from '../api/auth-api'
import Toast from '../components/Toast'

// Validação

import { emailValidator } from '../helpers/emailValidator'
import { passwordValidator } from '../helpers/passwordValidator'
import { nameValidator } from '../helpers/nameValidator'
import { telefoneValidator } from '../helpers/telefoneValidator'
import { cidadeValidator } from '../helpers/cidadeValidator'
import { convenioValidator } from '../helpers/convenioValidator'

export default function RegisterPaciente({ navigation }) {
  const [name, setName] = useState({ value: '', error: '' })
  const [email, setEmail] = useState({ value: '', error: '' })
  const [telefone, setTelefone] = useState({ value: '', error: '' })
  const [cidade, setCidade] = useState({ value: [], error: '' })
  const [convenio, setConvenio] = useState({ value: [], error: '' })
  const [password, setPassword] = useState({ value: '', error: '' })
  const [loading, setLoading] = useState()
  const [error, setError] = useState()

  const onSelectedCidadesChange = (selectedItems) => {
    setCidade({ value: selectedItems })
  }

  const onSelectedConveniosChange = (selectedItems) => {
    setConvenio({ value: selectedItems })
  }

  const onSignUpPressed = async () => {
    const nameError = nameValidator(name.value)
    const emailError = emailValidator(email.value)
    const telefoneError = telefoneValidator(telefone.value)
    const cidadeError = cidadeValidator(cidade.value)
    const convenioError = convenioValidator(convenio.value)
    const passwordError = passwordValidator(password.value)
    if (emailError || passwordError || nameError || telefoneError) {
      setName({ ...name, error: nameError })
      setEmail({ ...email, error: emailError })
      setTelefone({ ...telefone, error: telefoneError })
      setCidade({ ...cidade, error: cidadeError })
      setConvenio({ ...convenio, error: convenioError })
      setPassword({ ...password, error: passwordError })
      return
    }
    setLoading(true)
    const response = await signUpUser({
      nome: name.value,
      email: email.value,
      password: password.value,
      userType: 'paciente',
      telefone: telefone.value,
      convenio: convenio.value,
      cidade: cidade.value,
    })
    if (response.error) {
      setError(response.error)
    }
    setLoading(false)
  }

  return (
    <Background>
      <Header>Preencha os dados abaixo para criar o seu cadastro</Header>
      <TextInput
        labelOut="Nome"
        returnKeyType="next"
        value={name.value}
        onChangeText={(text) => setName({ value: text, error: '' })}
        error={!!name.error}
        errorText={name.error}
      />
      <TextInput
        labelOut="E-mail"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <TextInput
        labelOut="Telefone"
        placeholder="(99) 99999-9999"
        returnKeyType="next"
        value={telefone.value}
        onChangeText={(text) => setTelefone({ value: text, error: '' })}
        error={!!telefone.error}
        errorText={telefone.error}
        autoCapitalize="none"
        autoCompleteType="tel"
        keyboardType="phone-pad"
        textContentType="telephoneNumber"
      />
      <SelectFilter
        labelOut="Convênio"
        items={convenios}
        onSelectedItemsChange={onSelectedConveniosChange}
        selectText=""
        showChips={false}
        single
        selectedItems={convenio.value}
        readOnlyHeadings
        showDropDowns={false}
      />
      <SelectFilter
        labelOut="Cidade"
        items={estados}
        onSelectedItemsChange={onSelectedCidadesChange}
        selectText=""
        showDropDowns
        showChips={false}
        readOnlyHeadings
        single
        selectedItems={cidade.value}
      />
      <TextInput
        labelOut="Senha de acesso"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <Button
        loading={loading}
        mode="contained"
        onPress={onSignUpPressed}
        style={{ marginTop: 24, backgroundColor: '#fd7304' }}
      >
        Criar meu cadastro
      </Button>

      <View style={styles.row}>
        <Text>Já tem uma conta? </Text>
        <TouchableOpacity onPress={() => navigation.replace('LoginScreen')}>
          <Text style={styles.link}>Login</Text>
        </TouchableOpacity>
      </View>
      <Toast message={error} onDismiss={() => setError('')} />
    </Background>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
})
