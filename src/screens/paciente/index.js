// Médicos
export { default as Home } from './Home'

export { default as Agenda } from './Agenda'
export { default as Perfil } from './Perfil'
export { default as PacienteConsultas } from './PacienteConsultas'
export { default as PedirConsultaPaciente } from './PedirConsulta'
