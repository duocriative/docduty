import React from 'react'
import { StyleSheet, View } from 'react-native'
import Header from '../../components/Header'
import Button from '../../components/Button'
import Paragraph from '../../components/Paragraph'
import TopBar from '../../components/TopBar'
import Background from '../../components/Background'
import CardConsulta from '../../components/CardConsulta'
import { logoutUser } from '../../api/auth-api'

export default function Home({ navigation }) {
  return (
    <Background>
      <TopBar />
      <Button mode="contained" onPress={logoutUser} style={{ width: 120 }}>
        Logout
      </Button>
      <Header>Bem vindo ao Doc Duty</Header>
      <Paragraph>
        Solicite uma consulta que um médico entrará em contato
      </Paragraph>
      <Button
        mode="contained"
        onPress={() => navigation.navigate('PedirConsultaPaciente')}
        style={{ marginTop: 24, backgroundColor: '#fd7304' }}
      >
        Solicitar uma consulta
      </Button>
      <View>
        <Header>Minhas consultas</Header>
        {/*<CardConsulta
          title="Dor nas costas dsdfds fegdfsgfd fegvfd"
          button="Visualizar"
        />*/}
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
