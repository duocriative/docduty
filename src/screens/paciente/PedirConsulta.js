import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'
import DropDown from '../../components/DropDown'
import { firebase, db } from '../../firebase/config'
import DatePicker from '../../components/DatePicker'
import Background from '../../components/Background'
import Header from '../../components/Header'
import TopBar from '../../components/TopBar'
import Button from '../../components/Button'
import TextInput from '../../components/TextInput'
import SwitchButton from '../../components/SwitchButton'
//import TagsInput from '../../components/TagsInput'
import TagInput from 'react-native-tags-input';
import { theme } from '../../core/theme'

import Toast from '../../components/Toast'

export default function PedirConsultaPaciente({ navigation }) {
  const { uid, displayName } = firebase.auth().currentUser
  const initalState = {
    user_id: uid,
    user_name: displayName,
    titulo: { value: '', error: '' },
    detalhes: { value: '', error: '' },
  }

  const [state, setState] = useState(initalState)

  const handleChangeState = (value, name) => {
    setState({ ...state, [name]: value, show: false })
  }

  const [loading, setLoading] = useState()
  const [error, setError] = useState()
  const [returnMsg, setReturnMsg] = useState()
  const [userdados, setUserDados] = useState([])

  useEffect(() => {
    db.collection('users')
      .doc(uid)
      .get()
      .then((item) => {
        //setPerfil(lista)
        setUserDados(item.data())
      })
  }, [])

  const salvarForm = async () => {
    setLoading(true)
    try {
      await db.collection('consultas').add({
        user_id: state.user_id,
        user_name: userdados.nome,
        titulo: state.titulo.value,
        detalhes: state.detalhes.value,
        telefone: userdados.telefone,
        convenio: "Unimed Leste Fluminense",
        Cidade: "Niterói",
        user_rel: ""
      })
      setLoading(false)
      // setReturnMsg('Plantão criado com sucesso')
      navigation.navigate("Home")
      alert('Consulta criada com sucesso')
      
      // props.navigation.navigate('UsersList')
    } catch (returnError) {
      console.log(returnError)
      setError(returnError.message)
    }
  }

  const updateTagState = (value) => {
    //setState({tags: value})
    setState({ ...state, tags: value })
    console.log(state.tags)
  };

  return (
    <Background>
      <TopBar />
      <Header>Anunciar plantão</Header>
      <TextInput
        labelOut="Qual motivo da consulta"
        returnKeyType="next"
        value={state.titulo.value}
        onChangeText={(val) => {
          handleChangeState({ value: val, error: '' }, 'titulo')
        }}
        error={!!state.titulo.error}
        errorText={state.titulo.error}
      />

      <TextInput
        labelOut="Descreva em linhas gerais o seu problema"
        returnKeyType="next"
        multiline
        value={state.detalhes.value}
        onChangeText={(val) => {
          handleChangeState({ value: val, error: '' }, 'detalhes')
        }}
        error={!!state.detalhes.error}
        errorText={state.detalhes.error}
      />

      <Button
        loading={loading}
        mode="contained"
        onPress={salvarForm}
        style={{ marginTop: 24, backgroundColor: '#fd7304' }}
      >
        Pedir consulta
      </Button>

      <Toast message={error} onDismiss={() => setError('')} />
    </Background>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  tagsInput: {
    backgroundColor: '#fff',
    borderColor: '#8D8D8D',
    borderRadius: 4,
    borderWidth: 1,
    marginLeft: 0,
    padding: 6,
    width: '100%'
  }
})
