import React, { useState, useEffect } from 'react'
import {
  KeyboardAvoidingView,
  TouchableOpacity,
  Text,
  View,
  ImageBackground,
  StyleSheet,
  Platform,
} from 'react-native'
import { css } from '../../assets/css/Css'
import Logo from '../../components/Logo'
import Button from '../../components/Button'
import TextInput from '../../components/TextInput'
import { theme } from '../../core/theme'
import { emailValidator } from '../../helpers/emailValidator'
import { passwordValidator } from '../../helpers/passwordValidator'
import { loginUser, getUserInfo } from '../../api/auth-api'
import Toast from '../../components/Toast'

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState({ value: '', error: '' })
  const [password, setPassword] = useState({ value: '', error: '' })
  const [loading, setLoading] = useState()
  const [error, setError] = useState()
  const [user, setUser] = useState()

  const onLoginPressed = async () => {
    const emailError = emailValidator(email.value)
    const passwordError = passwordValidator(password.value)
    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError })
      setPassword({ ...password, error: passwordError })
      return
    }
    setLoading(true)
    const response = await loginUser({
      email: email.value,
      password: password.value,
    })
    if (response.error) {
      setError(response.error)
    } else {
      // const { uid } = response.user.user
      // const userdata = await getUserInfo(uid)
      // console.log(userdata)
      // console.log(getUserData())
      // storeData('userData', )
    }
    // console.log(user)
    setLoading(false)
  }

  return (
    <ImageBackground
      source={require('../../assets/background.png')}
      style={css.backGroundImage}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={css.container}
      >
        <View style={css.containerCenter}>
          <Logo />

          <Text style={styles.header}>Vamos começar?</Text>
          <TextInput
            labelOut="E-mail"
            returnKeyType="next"
            value={email.value}
            onChangeText={(text) => setEmail({ value: text, error: '' })}
            error={!!email.error}
            errorText={email.error}
            autoCapitalize="none"
            autoCompleteType="email"
            textContentType="emailAddress"
            keyboardType="email-address"
            style={styles.label}
            placeholder="Email"
          />
          <TextInput
            labelOut="Senha"
            returnKeyType="done"
            value={password.value}
            onChangeText={(text) => setPassword({ value: text, error: '' })}
            error={!!password.error}
            errorText={password.error}
            secureTextEntry
            style={styles.label}
            placeholder="Senha"
          />
        </View>

        <View style={styles.forgotPassword}>
          <TouchableOpacity
            onPress={() => navigation.navigate('ResetPassword')}
          >
            <Text style={styles.forgot}>Esqueci minha senha</Text>
          </TouchableOpacity>
          <Button
            loading={loading}
            mode="contained"
            onPress={onLoginPressed}
            style={styles.loginButton}
          >
            Entrar
          </Button>
        </View>

        <Button
          mode="contained"
          onPress={() => navigation.navigate('RegisterMedico')}
        >
          Fazer Cadastro
        </Button>
        <Toast message={error} onDismiss={() => setError('')} />
      </KeyboardAvoidingView>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  header: {
    color: '#fff',
    fontSize: 20,
    marginVertical: 25,
  },
  forgotPassword: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  forgot: {
    margin: 0,
    fontSize: 13,
    color: '#fff',
    textDecorationLine: 'underline',
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  loginButton: {
    margin: 0,
    backgroundColor: theme.colors.secondary,
    width: '40%',
  },
  label: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#fff',
  },
})
