// export { default as StartScreen } from './StartScreen'
export { default as Login } from './Login'
export { default as Register } from './Register'
export { default as RegisterMedico } from './RegisterMedico'
export { default as RegisterPaciente } from './RegisterPaciente'
export { default as ResetPassword } from './ResetPassword'
export { default as AuthLoading } from './AuthLoading'
