import React, { useState, useEffect, useRef} from 'react'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  SafeAreaView, 
  ScrollView,
  Dimensions,
  Modal
} from 'react-native'
import { AntDesign } from '@expo/vector-icons';
import { Text, Checkbox } from 'react-native-paper'
import { Camera } from 'expo-camera'
import * as ImagePicker from 'expo-image-picker'
import DropDown from '../../components/DropDown'
import Background from '../../components/Background'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TextInput from '../../components/TextInput'
import { estados } from '../../helpers/estados-cidades.json'
// import { convenios } from '../../helpers/convenios.json'
//import { opcaosexo } from '../../helpers/sexo.json'
import SelectFilter from '../../components/SelectFilter'
import { theme } from '../../core/theme'

import { signUpUser } from '../../api/auth-api'
import Toast from '../../components/Toast'

// Validação

import { emailValidator } from '../../helpers/emailValidator'
import { passwordValidator } from '../../helpers/passwordValidator'
import { nameValidator } from '../../helpers/nameValidator'
import { telefoneValidator } from '../../helpers/telefoneValidator'
import { cidadeValidator } from '../../helpers/cidadeValidator'
import { convenioValidator } from '../../helpers/convenioValidator'

export default function RegisterMedico({ navigation }) {
  const camRef = useRef(null)
  const [checked, setChecked] = useState(false);
  const [name, setName] = useState({ value: '', error: '' })
  const [email, setEmail] = useState({ value: '', error: '' })
  const [telefone, setTelefone] = useState({ value: '', error: '' })
  const [cidade, setCidade] = useState({ value: [], error: '' })
  const [convenio, setConvenio] = useState({ value: [], error: '' })
  const [espcialidade, setEspecialidade] = useState({ value: [], error: '' })
  const [sexo, setSexo] = useState({ value: [], error: '' })
  const [password, setPassword] = useState({ value: '', error: '' })
  const [loading, setLoading] = useState()
  const [error, setError] = useState()
  const [imgPerfil, setImgPerfil] = useState(null)
  const [imgCRM, setImgCRM] = useState(null)
  const [crm, setCrm] = useState({ value: '', error: '' })
  const [hasPermission, setHasPermission] = useState(null)
  const [capturedPhoto, setcapturedPhoto] = useState(null)
  const [type, setType] = useState('front');
  const [open, setOpen] = useState(false)

  /*
  const pickImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1,
    })
    console.log(result)

    if (!result.cancelled) {
      setImgCRM(result.uri)
    }
  }

  const profileImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1,
    })

    if (!result.cancelled) {
      setImgPerfil(result.uri)
    }
  }
  */

  const pickImage = async () => {
    const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
      setType('back')
  }

  const profileImage = async () => {
    const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
      type === 'back' ? setType('front') : setType('front')
  }

  const takePicture = async () => {
    if(camRef){
      const data = await camRef.current.takePictureAsync()
      setcapturedPhoto(data.uri)
      setOpen(true)
      //console.log(data)
    }
  }

  const savePicture = async (status) => {
    setHasPermission(status === null)
  }

  const onSelectedCidadesChange = (selectedItems) => {
    setCidade({ value: selectedItems })
  }

  const onSelectedSexoChange = (selectedItems) => {
    setSexo({ value: selectedItems })
  }

  const opcaosexo = [
    { label: 'Feminino', value: 'feminino' },
    { label: 'Masculino', value: 'masculino' },
    { label: 'Prefiro não dizer', value: 'prefiro não dizer' },
  ]

  const onSignUpPressed = async () => {
    const nameError = nameValidator(name.value)
    const emailError = emailValidator(email.value)
    const telefoneError = telefoneValidator(telefone.value)
    const crmError = ''
    const cidadeError = cidadeValidator(cidade.value)
    const convenioError = convenioValidator(convenio.value)
    const passwordError = passwordValidator(password.value)
    if (emailError || passwordError || nameError || telefoneError) {
      setName({ ...name, error: nameError })
      setEmail({ ...email, error: emailError })
      setTelefone({ ...telefone, error: telefoneError })
      setCidade({ ...cidade, error: cidadeError })
      setSexo({ ...sexo, error: cidadeError })
      setConvenio({ ...convenio, error: convenioError })
      setPassword({ ...password, error: passwordError })
      setCrm({ ...crm, error: crmError })
      setImgPerfil({ ...imgPerfil, error: crmError })
      return
    }
    setLoading(true)
    const response = await signUpUser({
      nome: name.value,
      email: email.value,
      password: password.value,
      userType: 'medico',
      telefone: telefone.value,
      crm: crm.value,
      sexo: sexo.value,
      cidade: cidade.value,
      imgPerfil: imgPerfil,
      imgCRM: imgCRM,
    })
    if (response.error) {
      setError(response.error)
    }
    setLoading(false)
  }

  return (
    <View style={{flex: 1}}>
      {hasPermission === true && (
        <SafeAreaView style={styles.containerCamera}>
          <TouchableOpacity style={styles.btnClose} onPress={ (status) => setHasPermission(status === null)}>
              <AntDesign name="closecircle" size={24} color="#fd7304" />
            </TouchableOpacity>
            <Camera style={styles.camera} type={type} ref={camRef}>
            </Camera>

            <View style={styles.containerButtons}>
              <TouchableOpacity style={styles.btnCamera} onPress={takePicture}>
                <AntDesign name="camera" size={24} color="#fff" /> 
                <Text style={styles.btnColor}>
                  Capturar Imagem
                </Text>
              </TouchableOpacity>
            </View>

          {capturedPhoto && (
            <Modal
            animationType='slide'
            transparent={true}
            visible={open}>
              <View style={styles.modalContainer}>
                <Image style={styles.modalImage} source={{uri: capturedPhoto}} />
                <View style={styles.containerButtons}>
                  <TouchableOpacity onPress={takePicture} onPress={ () => { setcapturedPhoto(null) } }>
                    <AntDesign name="camera" size={44} color="#fd7304" />
                  </TouchableOpacity> 

                  <TouchableOpacity style={styles.btnConfirm} onPress={savePicture}>
                    <AntDesign name="checkcircle" size={32} color="#fd7304" />
                  </TouchableOpacity>
                </View>
                
              </View>
            </Modal>
          )}
        </SafeAreaView>)}
      <Background>
        <Header>Preencha os dados abaixo para criar o seu cadastro</Header>
        <TextInput
          labelOut="Nome"
          returnKeyType="next"
          value={name.value}
          onChangeText={(text) => setName({ value: text, error: '' })}
          error={!!name.error}
          errorText={name.error}
        />
        <TextInput
          labelOut="E-mail"
          returnKeyType="next"
          value={email.value}
          onChangeText={(text) => setEmail({ value: text, error: '' })}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
        />
        <TextInput
          labelOut="Telefone"
          placeholder="(99) 99999-9999"
          returnKeyType="next"
          value={telefone.value}
          onChangeText={(text) => setTelefone({ value: text, error: '' })}
          error={!!telefone.error}
          errorText={telefone.error}
          autoCapitalize="none"
          autoCompleteType="tel"
          keyboardType="phone-pad"
          textContentType="telephoneNumber"
        />

        <DropDown
          labelOut="Sexo"
          valor={sexo.value}
          itens={opcaosexo}
          placeholder=""
          onChangeValue={(val) => {
            onSelectedSexoChange(val)
          }}
          error={!!sexo.error}
          errorText={sexo.error}
        />

        <View style={styles.flexContainer}>
          <TextInput
            labelOut="CRM"
            placeholder=""
            returnKeyType="next"
            value={crm.value}
            onChangeText={(text) => setCrm({ value: text, error: '' })}
            error={!!crm.error}
            errorText={crm.error}
            autoCapitalize="none"
          />

          <Button
            loading={loading}
            mode="contained"
            onPress={pickImage}
            style={{ backgroundColor: theme.colors.secondary }}>
            Enviar foto
          </Button>
        </View>

        <Text>Enviar foto do seu CRM para comprovação</Text>

        <SelectFilter
          labelOut="Cidade"
          items={estados}
          onSelectedItemsChange={onSelectedCidadesChange}
          selectText=""
          showDropDowns
          showChips={false}
          readOnlyHeadings
          single
          selectedItems={cidade.value}
        />

        <View style={styles.flexContainer}>
          <TextInput
            labelOut="Perfil"
            placeholder=""
            returnKeyType="next"
            autoCapitalize="none"
          />

          <Button
            loading={loading}
            mode="contained"
            onPress={profileImage}
            style={{ backgroundColor: theme.colors.secondary }}>
            Enviar foto
          </Button>
        </View>

        <Text>Esta foto não ficará gravada ou utilizada como perfil do usuário, serve apenas para identificação do mesmo.</Text>

        <TextInput
          labelOut="Senha de acesso"
          returnKeyType="done"
          value={password.value}
          onChangeText={(text) => setPassword({ value: text, error: '' })}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry
        />

        <Text style={styles.labelScrollview}>Termos de uso</Text>

        <ScrollView style={styles.scrollview}>
          <Text style={styles.scrollviewText}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
              minim veniam, quis nostrud exercitation ullamco laboris nisi ut
              aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
              pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum.
          </Text>
        </ScrollView>

        <View style={styles.checkboxContainer}>
          <Checkbox
            status={checked ? 'checked' : 'unchecked'}
            onPress={() => {
              setChecked(!checked);
            }}
          />
          <Text style={styles.label}>Estou de acordo com os termos de uso</Text>
        </View>
        

        <Button
          loading={loading}
          mode="contained"
          onPress={onSignUpPressed}
          style={{ marginTop: 24, backgroundColor: '#fd7304' }}
        >
          Criar meu cadastro
        </Button>

        <View style={styles.row}>
          <Text>Já tem uma conta? </Text>
          <TouchableOpacity onPress={() => navigation.replace('Login')}>
            <Text style={styles.link}>Login</Text>
          </TouchableOpacity>
        </View>
        <Toast message={error} onDismiss={() => setError('')} />
      </Background>
    </View>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  scrollview: {
    backgroundColor: "#fff",
    borderRadius: 10,
    color: "#d8d8d8",
    height: 140,
    marginTop: 16,
    padding: 12,
  },
  scrollviewText: {
    color: "#484848",
    fontSize: 12,
    lineHeight: 20
  },
  labelScrollview: {
    fontWeight: 'bold',
    color: theme.colors.primary,
    marginTop: 12
  },
  /*flexContainer: {
    flexDirection: "row"
  },*/
  checkboxContainer: {
    flexDirection: "row",
    marginTop: 20,
  },
  checkbox: {
    alignSelf: "center",
    backgroundColor: '#fff',
    borderColor: '#fff',
    padding: 8
  },
  label: {
    margin: 8,
  },
  camera: {
    height: Dimensions.get('window').height/2.25,
    marginTop: 80,
    width: Dimensions.get('window').width/1.125,
    zIndex: 9
  },
  containerCamera: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    height: Dimensions.get('window').height,
    position: 'absolute',
    top: 0,
    textAlign: 'center',
    width: Dimensions.get('window').width,
    zIndex: 10
  },
  btnClose: {
    borderRadius: 12,
    right: 20,
    top: 20,
    color: '#fff',
    padding: 16,
    position: 'absolute'
  },
  btnCamera: {
    backgroundColor: '#fd7304',
    borderRadius: 12,
    color: '#fff',
    flexDirection: 'row',
    padding: 16,
  },
  btnConfirm: {
    //backgroundColor: '#fd7304',
    borderRadius: 12,
    color: '#fff',
    flexDirection: 'row',
    marginLeft: 24,
    position: 'relative',
    top: 6
  },
  btnColor: {
    color: '#fff',
    left: 6,
    marginRight: 4,
    position: 'relative',
    top: 4
  },
  modalContainer: {
    alignItems: "center",
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
    padding: 24
  },
  modalImage: {
    height: 360,
    width: '100%'
  },
  containerButtons: {
    bottom: 80,
    flexDirection: 'row',
    marginTop: 40,
    position: 'absolute'
  }
})
