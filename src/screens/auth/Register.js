import React from 'react'
import {
  ImageBackground,
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
} from 'react-native'
import { Text } from 'react-native-paper'
import { css } from '../../assets/css/Css'
import Logo from '../../components/Logo'
import Button from '../../components/Button'
import { theme } from '../../core/theme'

export default function RegisterScreen({ navigation }) {
  return (
    <ImageBackground
      source={require('../../assets/background.png')}
      style={css.backGroundImage}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={css.container}
      >
        <View style={css.containerCenter}>
          <Logo />
          <Text style={styles.header}>Escolha um tipo de cadastro:</Text>
          <Button
            mode="contained"
            onPress={() => navigation.navigate('RegisterMedico')}
            style={{ marginTop: 24, backgroundColor: theme.colors.secondary }}
          >
            Sou um médico
          </Button>
          <Button
            mode="contained"
            onPress={() => navigation.navigate('RegisterPaciente')}
            style={{ marginTop: 24, backgroundColor: '#fd7304' }}
          >
            Sou um paciente
          </Button>
        </View>
      </KeyboardAvoidingView>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    backgroundColor: theme.colors.primary,
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
    justifyContent: 'center',
  },
  header: {
    color: '#fff',
    fontSize: 20,
    marginVertical: 25,
  },
  container: {
    flex: 1,
    padding: 20,
    width: '100%',
    maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
