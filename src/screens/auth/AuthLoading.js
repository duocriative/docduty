import React, { useEffect, useState } from 'react'
import { ActivityIndicator } from 'react-native'
import { firebase, db } from '../../firebase/config'
import { getUserData, getUserInfo } from '../../api/auth-api'
import Background from '../../components/Background'
import { theme } from '../../core/theme'


export default function AuthLoading({ navigation }) {
  const [userType, setUserType] = useState([])
  const [userUid, setUserUid] = useState()
  const [isLoggedIn, setIsLoggedIn] = useState(false)

  firebase.auth().onAuthStateChanged((user) => {
    if (user) {

      const userUid = () => {
        db.collection('users')
        .doc(user.uid)
        .get()
        .then((item) => {
          let doc = item.data()

          let home = doc.userType === 'medico' ? 'HomeMedico' : 'HomePaciente'

          navigation.reset({
            index: 0,
            routes: [{ name: home }],
          })

        })
      }
  
      userUid()

    } else {
      // User is not logged in
      navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }],
      })
    }
  })
  return (
    <Background>
      <ActivityIndicator size="large" color={theme.colors.primary} />
    </Background>
  )
}
