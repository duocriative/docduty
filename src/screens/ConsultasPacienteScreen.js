import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import Header from '../components/Header'
import Button from '../components/Button'
import TopBar from '../components/TopBar'
import Background from '../components/Background'
import CardConsulta from '../components/CardConsulta'

import { theme } from '../core/theme'

export default function ConsultasPacienteScreen({ navigation }) {
  return (
    <Background>
      <TopBar />
      <Header>Minha agenda</Header>
      <Text style={styles.subtitulo}>Fevereiro 2020</Text>
      <View>
        <CardConsulta
          title="Dor nas costas"
          button="Visualizar"
          navigation={navigation}
        />
      </View>
      <Button
        mode="contained"
        onPress={() => navigation.navigate('AgendaPacienteScreen')}
        style={{ marginTop: 24, backgroundColor: '#fd7304' }}
      >
        Voltar para agenda
      </Button>
    </Background>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subtitulo: {
    fontWeight: 'bold',
    fontSize: 36,
    color: theme.colors.text1,
  },
})
