import React, { useState } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Card, Title, Paragraph, Subheading } from 'react-native-paper'
import Header from '../components/Header'
import Button from '../components/Button'
import TopBar from '../components/TopBar'
import Background from '../components/Background'
import MetaInfoIcon from '../components/MetaInfoIcon'
import TextInput from '../components/TextInput'
import { getUserData } from '../api/auth-api'

import { theme } from '../core/theme'

export default function ConsultaScreen() {
  const user = getUserData()
  return (
    <Background>
      <TopBar />
      <Header>Dor nas costas e nas juntas</Header>
      <View>
        <Card style={styles.card}>
          <Card.Content style={styles.cardContent}>
            <MetaInfoIcon
              text="Neide Aparecida dos Santos / 65"
              icon="AccountOutline"
              style={{ text: { fontWeight: 'bold', fontSize: 18 } }}
            />
            <MetaInfoIcon text="Niterói (RJ)" icon="MapMarkerOutline" />
            <MetaInfoIcon
              text="(21) 9999-9999"
              icon="Whatsapp"
              fillIcon="#117d06"
            />
            <View style={styles.consultaDetalhesContent}>
              <Subheading
                style={{ fontSize: 18, color: '#000', fontWeight: 'bold' }}
              >
                Subheading
              </Subheading>
              <Paragraph>
                Lorem Ipsum has been the industry's standard dummy text ever
                since the 1500s, when an unknown printer took a galley of type
                and scrambled it to make a type specimen book. It has survived
                not only five centuries, remaining essentially unchanged.
              </Paragraph>
            </View>
          </Card.Content>
        </Card>
      </View>
      {user && user.userType === 'medico' ? (
        <View>
          <TextInput
            labelOut="Envie uma mensagem para o paciente"
            returnKeyType="next"
            multiline
          />
          <Button
            mode="contained"
            // onPress={() => navigation.navigate('AgendaPacienteScreen')}
            style={{ marginTop: 24, backgroundColor: '#fd7304' }}
          >
            Atender paciente
          </Button>
        </View>
      ) : null}
      {user && user.userType === 'medico' ? (
        <View style={styles.consultaDetalhesResposta}>
          <Subheading style={styles.consultaDetalhesRespostaHeading}>
            Consulta atendida
          </Subheading>
          <Paragraph style={{ color: '#fff' }}>
            Em breve você vai receber um contato do nosso médico:
          </Paragraph>
          <View style={{ color: '#fff' }}>
            <Text style={{ color: '#fff' }}>Dr. Gilberto</Text>
            <Text style={{ color: '#fff' }}>
              Pratz Telefone: (21) 9 9999-9999
            </Text>
            <Text style={{ color: '#fff' }}>E-mail: nonon@nonono.com</Text>
          </View>
        </View>
      ) : null}
    </Background>
  )
}

const styles = StyleSheet.create({
  card: {
    paddingTop: 14,
    backgroundColor: '#0472fd',
    borderRadius: 14,
    marginVertical: 11,
  },
  cardTitle: {
    fontWeight: 'bold',
  },
  cardContent: {
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    paddingLeft: 10,
    backgroundColor: '#fff',
    minHeight: 106,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  consultaDetalhesContent: {
    borderTopWidth: 1,
    borderTopColor: '#0472fd',
    marginVertical: 20,
    paddingVertical: 20,
  },
  consultaDetalhesResposta: {
    backgroundColor: '#000615',
    color: '#fff',
    width: '100vw',
    alignSelf: 'stretch',
  },
  consultaDetalhesRespostaHeading: {
    color: '#43c0ba',
    fontWeight: 'bold',
  },
})
