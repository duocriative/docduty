import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import {
  Calendar,
  CalendarList,
  Agenda,
  LocaleConfig,
} from 'react-native-calendars'
import Header from '../components/Header'
import Button from '../components/Button'
import TopBar from '../components/TopBar'
import Background from '../components/Background'

import { theme } from '../core/theme'

LocaleConfig.locales.ptBr = {
  monthNames: [
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro',
  ],
  monthNamesShort: [
    'Jan.',
    'Fev.',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul.',
    'Ago',
    'Set.',
    'Out.',
    'Nov.',
    'Dez.',
  ],
  dayNames: [
    'Domingo',
    'Segunda',
    'Terça',
    'Quarta',
    'Quinta',
    'Sexta',
    'Sábado',
  ],
  dayNamesShort: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
}

LocaleConfig.defaultLocale = 'ptBr'

export default function AgendaPacienteScreen({ navigation }) {
  return (
    <Background>
      <TopBar />
      <Header>Minha agenda</Header>
      <Calendar
        markedDates={{
          '2021-07-16': { selected: true },
          '2021-07-25': { selected: true },
        }}
        monthFormat="MMMM yyyy"
        // Specify style for calendar container element. Default = {}
        style={{
          padding: 0,
          backgroundColor: theme.colors.tint,
        }}
        // Specify theme properties to override specific styles for calendar parts. Default = {}
        theme={{
          arrowColor: 'white',
          textDayFontSize: 18,
          selectedDayBackgroundColor: '#fd7304',
          dayTextColor: '#000',
          todayTextColor: '#fd7304',
          'stylesheet.calendar.main': {
            monthView: {
              backgroundColor: 'white',
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
            },
          },
          'stylesheet.calendar.header': {
            header: {
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingLeft: 10,
              paddingRight: 10,
              marginTop: 6,
              marginBottom: 6,
              alignItems: 'center',
              backgroundColor: '#fd7e02',
              borderRadius: 10,
            },
            headerContainer: {
              flexDirection: 'row',
            },
            monthText: {
              fontSize: 36,
              fontWeight: 'bold',
              color: 'white',
              margin: 10,
            },
            week: {
              marginTop: 7,
              flexDirection: 'row',
              justifyContent: 'space-around',
              backgroundColor: 'white',
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
            },
            dayHeader: {
              marginTop: 2,
              marginBottom: 7,
              width: 32,
              fontSize: 24,
              fontWeight: 'bold',
              textAlign: 'center',
              color: '#0472fd',
            },
          },
        }}
      />
    </Background>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subtitulo: {
    fontWeight: 'bold',
    fontSize: 36,
    color: theme.colors.text1,
  },
})
