import React, { useState, useEffect } from 'react'
import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native'
import moment from 'moment'
import { Card, Title, Paragraph, Subheading } from 'react-native-paper'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TopBar from '../../components/TopBar'
import Background from '../../components/Background'
import MetaInfoIcon from '../../components/MetaInfoIcon'
import TextInput from '../../components/TextInput'
import { Stethoscope } from '../../assets/icons'
import { firebase, db } from '../../firebase/config'

export default function DetalhesConsulta({ route, navigation }) {
  const { uid } = firebase.auth().currentUser
  const [loading, setLoading] = useState(true)
  const [consulta, setConsulta] = useState()
  const { itemId } = route.params

  const getConsultaById = async (id) => {
    const dbRef = db.collection('consultas').doc(id)
    const doc = await dbRef.get()
    const consultaData = doc.data()
    setConsulta({ ...consultaData, id: doc.id })
    setLoading(false)
  }

  const updateConsulta = async () => {
    const userRef = db.collection('consultas').doc(itemId)
    // console.log(userRef)
    await userRef.update({
        user_rel: uid,
        status: 'Pedido em analise',
      })
      .then((retorno) => {
        alert('Solicitação enviada')
        console.log(retorno)
        // navigation.navigate('Home', { user: data })
      })
      .catch((error) => {
        alert(error)
      })
    // props.navigation.navigate("UsersList");
  }

  useEffect(() => {
    getConsultaById(itemId)
  }, [])
  // const user = getUserData()
  if (loading) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size="large" color="#9E9E9E" />
      </View>
    )
  }
  return (
    <Background>
      <TopBar />
      <Header style={styles.cardHead}>
        <Title style={styles.cardTitle}>
          {consulta.titulo} {consulta.tipo}
        </Title>
      </Header>
      <View>
        <Card style={styles.card}>
          <Card.Content style={styles.cardContent}>
            <MetaInfoIcon text={consulta.cidade} icon="MapMarkerOutline" />
            <View style={styles.cardMetaContent}>
              <MetaInfoIcon text={consulta.convenio} icon="CalendarMonthOutline" />
            </View>
            <MetaInfoIcon
              text={consulta.telefone}
              icon="Whatsapp"
              fillIcon="#117d06"
            />
            <View style={styles.consultaDetalhesContent}>
              <Subheading
                style={{ fontSize: 18, color: '#000', fontWeight: 'bold' }}
              >
                Detalhes da Consulta
              </Subheading>
              <Paragraph>{consulta.detalhes}</Paragraph>
            </View>

          </Card.Content>
        </Card>
      </View>
      <View>
        {
          consulta.user_rel && consulta.status == 'Aceito' ?
          <Button
            icon={({ size, color }) => <Stethoscope fill="#fff" />}
            mode="contained"
            onPress={() => updateConsulta()}
            style={{ marginTop: 24, backgroundColor: '#fd7304' }}
          >
            Navegar com waze
          </Button>
          : 
          <Button
            mode="contained"
            onPress={() => updateConsulta()}
            style={{ marginTop: 24, backgroundColor: '#fd7304' }}
          >
            Atender este Paciente
          </Button>
        }
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  card: {
    paddingTop: 14,
    backgroundColor: '#0472fd',
    borderRadius: 14,
    marginVertical: 11,
  },
  cardHead: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#b6b4b4',
    paddingVertical: 10,
    marginBottom: 10,
    width: '100%',
  },
  cardTitle: {
    fontWeight: 'bold',
    lineHeight: 40,
    textTransform: 'capitalize',
  },
  cardContent: {
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    paddingLeft: 10,
    backgroundColor: '#fff',
    minHeight: 106,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  cardMetaContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginBottom: 8,
    paddingTop: 10,
  },
  consultaDetalhesContent: {
    borderTopWidth: 1,
    borderTopColor: '#0472fd',
    marginVertical: 20,
    paddingVertical: 20,
    width: '100%',
  },
  consultaDetalhesResposta: {
    backgroundColor: '#000615',
    color: '#fff',
    width: '100vw',
    alignSelf: 'stretch',
  },
  consultaDetalhesRespostaHeading: {
    color: '#43c0ba',
    fontWeight: 'bold',
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 125,
    height: 27,
    borderRadius: 27,
    backgroundColor: '#fd7e02',
    display: 'flex',
    marginBottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 12,
    color: '#fff',
  },
  tagsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  tag: {
    backgroundColor: '#e2e2e2',
    borderRadius: 12,
    margin: 12,
    padding: 4,
  }
})
