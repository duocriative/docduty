import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TopBar from '../../components/TopBar'
import Background from '../../components/Background'
import CardConsulta from '../../components/CardConsulta'
import SearchInput from '../../components/SearchInput'
import moment from 'moment'
import { firebase, db } from '../../firebase/config'

import { theme } from '../../core/theme'

export default function ConsultasPacienteScreen({ navigation }) {
  const { uid } = firebase.auth().currentUser
  const [consultas, setConsultas] = useState([])
  const [name, setName] = useState({ value: '', error: '' })

  useEffect(() => {
    db.collection('consultas')
      .onSnapshot((querySnapshot) => {
        const lista = []
        querySnapshot.forEach((snapshot) => {
          const { titulo} =
            snapshot.data()
          lista.push({
            id: snapshot.id,
            titulo,
          })
        })
        setConsultas(lista)
      })
  }, [])

  return (
    <Background>
      <TopBar />
      <SearchInput
        placeholder="Pocure aqui seu plantão"
      />
      <View style={styles.headerLista}>
        <Header>Pedidos de consulta</Header>
      </View>
      {
      //<Text style={styles.subtitulo}>{moment(new Date()).format('MMMM')} {moment(new Date()).format('Y')}</Text>
      }
      <View>
      {consultas.map((consulta) => (
        <CardConsulta
          key={consulta.id}
          title={consulta.titulo}
          button="Visualizar"
          onPress={() => {
            navigation.navigate('DetalhesConsulta', {
              itemId: consulta.id,
            })
          }}
        />
        ))}
      </View>
      <Button
        mode="contained"
        onPress={() => navigation.navigate('Agenda')}
        style={{ marginTop: 24, backgroundColor: '#fd7304' }}
      >
        Voltar para agenda
      </Button>
    </Background>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subtitulo: {
    fontWeight: 'bold',
    fontSize: 36,
    color: theme.colors.text1,
  },
  headerLista: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  filterButton: {
    color: theme.colors.primary,
    textDecorationLine: 'underline'
  }
})
