import React, { useState, useEffect } from 'react'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from 'react-native'
import Background from '../../components/Background'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TextInput from '../../components/TextInput'
import DropDown from '../../components/DropDown'
import { estados } from '../../helpers/estados-cidades.json'
import { convenios } from '../../helpers/convenios.json'
import SelectFilter from '../../components/SelectFilter'
import { theme } from '../../core/theme'

import { logoutUser, getUserData } from '../../api/auth-api'
import Toast from '../../components/Toast'
import { firebase, db } from '../../firebase/config'


export default function EditarPerfil({ navigation }) {
  const [name, setName] = useState({ value: '', error: '' })
  const [email, setEmail] = useState({ value: '', error: '' })
  const [telefone, setTelefone] = useState({ value: '', error: '' })
  const [cidade, setCidade] = useState({ value: [], error: '' })
  const [convenio, setConvenio] = useState({ value: [], error: '' })
  const [sexo, setSexo] = useState({ value: [], error: '' })
  const [crm, setCrm] = useState({ value: '', error: '' })
  const [password, setPassword] = useState({ value: '', error: '' })
  const [loading, setLoading] = useState()
  const [error, setError] = useState()
  const { uid } = firebase.auth().currentUser
  const [perfil, setPerfil] = useState([])

  useEffect(() => {
    db.collection('users')
      .doc(uid)
      .get()
      .then((item) => {
        //setPerfil(lista)
        setPerfil(item.data())
      })
  }, [])

  name.value = perfil.nome
  email.value = perfil.email
  telefone.value = perfil.telefone
  sexo.value = perfil.sexo
  crm.value = perfil.crm
  cidade.value = perfil.cidade

  const onSelectedCidadesChange = (selectedItems) => {
    setCidade({ value: selectedItems })
  }

  const onSelectedSexoChange = (selectedItems) => {
    setSexo({ value: selectedItems })
  }

  const opcaosexo = [
    { label: 'Feminino', value: 'feminino' },
    { label: 'Masculino', value: 'masculino' },
    { label: 'Prefiro não dizer', value: 'prefiro não dizer' },
  ]

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Background>
          <Header>
            Preencha os dados abaixo para editar o seu perfil no Doc Duty
          </Header>
          <Button mode="contained" onPress={logoutUser} style={{ width: 120 }}>
            Logout
          </Button>
          <TextInput
            labelOut="Nome"
            returnKeyType="next"
            value={name.value}
            onChangeText={(text) => setName({ value: text, error: '' })}
            error={!!name.error}
            errorText={name.error}
          />
          <TextInput
            labelOut="E-mail"
            returnKeyType="next"
            value={email.value}
            onChangeText={(text) => setEmail({ value: text, error: '' })}
            error={!!email.error}
            errorText={email.error}
            autoCapitalize="none"
            autoCompleteType="email"
            textContentType="emailAddress"
            keyboardType="email-address"
          />
          <TextInput
            labelOut="Telefone"
            placeholder="(99) 99999-9999"
            returnKeyType="next"
            value={telefone.value}
            onChangeText={(text) => setTelefone({ value: text, error: '' })}
            error={!!telefone.error}
            errorText={telefone.error}
            autoCapitalize="none"
            autoCompleteType="tel"
            keyboardType="phone-pad"
            textContentType="telephoneNumber"
          />
          <SelectFilter
            labelOut="Cidade"
            items={estados}
            onSelectedItemsChange={onSelectedCidadesChange}
            selectText=""
            placeholder={cidade.value}
            showDropDowns
            showChips={false}
            readOnlyHeadings
            single
            selectedItems={cidade.value}
          />
          <DropDown
            labelOut="Sexo"
            valor={sexo.value}
            itens={opcaosexo}
            placeholder={sexo.value}
            onChangeValue={(val) => {
              onSelectedSexoChange(val)
            }}
            error={!!sexo.error}
            errorText={sexo.error}
          />
          <TextInput
            labelOut="CRM"
            placeholder=""
            returnKeyType="next"
            value={crm.value}
            onChangeText={(text) => setCrm({ value: text, error: '' })}
            error={!!crm.error}
            errorText={crm.error}
            autoCapitalize="none"
          />
          <TextInput
            labelOut="Senha de acesso"
            returnKeyType="done"
            value={password.value}
            onChangeText={(text) => setPassword({ value: text, error: '' })}
            error={!!password.error}
            errorText={password.error}
            secureTextEntry
          />
          <Button
            loading={loading}
            mode="contained"
            // onPress={onSignUpPressed}
            style={{ marginTop: 24, backgroundColor: '#fd7304' }}
          >
            Atualizar meu cadastro
          </Button>

          <Toast message={error} onDismiss={() => setError('')} />
        </Background>
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
})
