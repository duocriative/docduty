import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import moment from 'moment'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TopBar from '../../components/TopBar'
import Background from '../../components/Background'
import CardPlantao from '../../components/CardPlantao'
import { firebase, db } from '../../firebase/config'
import { fetchApiData } from '../../api/api'

import { theme } from '../../core/theme'

export default function Anuncios({ navigation }) {
  const { uid } = firebase.auth().currentUser
  const [plantoes, setPlantoes] = useState([])
  const [especialidades, setEspecialidades] = useState([])

  async function fetchEspecialidadesJSON() {
    const response = await fetch('https://docduty.com.br/aplicativo/wp-json/wp/v2/especialidades?_embed');
    const esp = await response.json();
    const arr = []
    esp.forEach(el => {
      const { especialidade, cor } = el
      arr.push({
        especialidade: el.title.rendered,
        cor: el.acf.cor
      })
    })
    return arr
  }

  useEffect(() => {
    fetchEspecialidadesJSON().then(vl => {
      db.collection('plantoes')
      .where('user_id', '==', uid)
      .onSnapshot((querySnapshot) => {
        const lista = []
        querySnapshot.forEach((snapshot) => {
          const { titulo, data, hora, local, tipo, especialidade, cor, user_name, status, user_id } =
            snapshot.data()

            function isEspecialidade(value) {
              return value.especialidade == especialidade
            }
            let especialidades = vl
            const filtered = especialidades.filter(isEspecialidade)

            lista.push({
              id: snapshot.id,
              titulo,
              dia: moment(data.toDate()).format('DD/MM'),
              hora,
              cor: filtered[0].cor,
              local,
              tipo,
              status,
              especialidade,
              user_name,
            })
        })
        setPlantoes(lista)
      })
    });
  }, [])

  return (
    <Background>
      <TopBar />
      <View style={styles.headerLista}>
        <Header>Meus Anúncios</Header>
      </View>
      <View style={styles.btnLista}>
        <Button
          mode="contained"
          onPress={() => navigation.navigate('CriarPlantao')}
          style={{ width: 225, marginLeft: 'auto', marginRight: 'auto', backgroundColor: '#fd7304' }}
        >
          + Anunciar Plantão
        </Button>
      </View>
      <View>
        <Text style={{marginTop: 24}}>Veja abaixo os anúncios que você criou.</Text>
      </View>
      
      <View>
      {plantoes.map((plantao) => (
          <CardPlantao
            key={plantao.id}
            title={plantao.titulo}
            button={plantao.especialidade}
            cor={plantao.cor}
            dia={plantao.dia}
            hora={plantao.hora}
            status={plantao.status}
            onPress={() => {
              navigation.navigate('DetalhesPlantaoAnuncio', {
                itemId: plantao.id,
                cor: plantao.cor
              })
            }}
          />
        ))}
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  headerLista: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subtitulo: {
    fontWeight: 'bold',
    fontSize: 36,
    color: theme.colors.text1,
  },
  btnLista: {
    textAlign: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  }
})
