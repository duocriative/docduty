import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text, ActivityIndicator } from 'react-native'
import { Divider } from 'react-native-paper'
import moment from 'moment'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TopBar from '../../components/TopBar'
import Background from '../../components/Background'
import CardPlantao from '../../components/CardPlantao'
import CardMedico from '../../components/CardMedico'
import { firebase, db } from '../../firebase/config'

import { theme } from '../../core/theme'

export default function DetalhesPlantaoAnuncio({ route, navigation }) {
  const { uid } = firebase.auth().currentUser
  const [loading, setLoading] = useState(true)
  const [plantao, setPlantao] = useState()
  const [user, setUser] = useState()
  const { itemId, cor, userRel } = route.params

  const getPlantaoById = async (id) => {
    const dbRef = db.collection('plantoes').doc(id)
    const doc = await dbRef.get()
    const plantaoData = doc.data()
    plantaoData.data = moment(plantaoData.data.toDate()).format('DD/MM')
    plantaoData.status =
      plantaoData.status === 'Pedido em analise'
        ? 'Aguardando resposta'
        : plantaoData.status
    setPlantao({ ...plantaoData, id: doc.id })
    if (plantaoData.user_rel) {
      getUserById(plantaoData.user_rel)
    }
    setLoading(false)
  }

  const getUserById = async (id) => {
    const dbRef = db.collection('users').doc(id)
    const doc = await dbRef.get()
    const userData = doc.data()
    setUser({ ...userData, id: doc.id })
    setLoading(false)
  }

  const negaPlantao = async () => {
    const userRef = db.collection('plantoes').doc(itemId)
    await userRef.update({
        user_rel: null,
        status: 'Aberto',
      })
      .then((retorno) => {
        setUser(null)
        alert('Solicitação negada')
      })
      .catch((error) => {
        alert(error)
      })
  }

  const aceitaPlantao = async () => {
    const userRef = db.collection('plantoes').doc(itemId)
    await userRef.update({
        status: 'Aceito',
      })
      .then((retorno) => {
        //setUser(null)
        alert('Solicitação aprovada')
      })
      .catch((error) => {
        alert(error)
      })
  }

  useEffect(() => {
    getPlantaoById(itemId)
    if (userRel) {
      getUserById(userRel)
    }
  }, [])
  if (loading) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size="large" color="#9E9E9E" />
      </View>
    )
  }
  return (
    <Background>
      <TopBar />
      <Header>Meus anúncios</Header>
      <View style={styles.container}>
        <CardPlantao
          key={plantao.id}
          title={plantao.titulo}
          button={plantao.especialidade}
          cor={cor}
          dia={plantao.data}
          hora={plantao.hora}
          status={plantao.status}
          user_rel={plantao.user_rel}
        />
        <Divider style={styles.divider} />
        {user && <CardMedico key={user.uid} nome={user.nome} crm={user.crm} />}
        {user &&
        <View style={styles.cardButtons}>
          <Button
            mode="contained"
            onPress={() => negaPlantao()}
            style={{ width: 100, fontSize: 8, marginRight: 16, backgroundColor: '#fd7304' }}
            >
            Negar
          </Button>
          <Button
            mode="contained"
            onPress={() => aceitaPlantao()}
            style={{ width: 100, fontSize: 12, backgroundColor: '#3DB7B1' }}
            >
            Aceitar
          </Button>
        </View>
        }
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  headerLista: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  subtitulo: {
    fontWeight: 'bold',
    fontSize: 36,
    color: theme.colors.text1,
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  divider: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 1,
    height: 30,
    color: '#000',
  },
  cardButtons: {
    flexDirection: 'row',
    marginTop: 24
  }
})
