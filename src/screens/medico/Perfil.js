import React, { useState, useEffect } from 'react'
import { View, StyleSheet } from 'react-native'
import { Paragraph } from 'react-native-paper'
import Background from '../../components/Background'
import Header from '../../components/Header'
import Button from '../../components/Button'
import { theme } from '../../core/theme'
import TopBar from '../../components/TopBar'
import CardNotificacao from '../../components/CardNotificacao'
import { firebase, db } from '../../firebase/config'

import { logoutUser, getUserData } from '../../api/auth-api'
import Toast from '../../components/Toast'

export default function ProfileScreen({ navigation }) {
  const [name, setName] = useState({ value: '', error: '' })
  const [email, setEmail] = useState({ value: '', error: '' })
  const [telefone, setTelefone] = useState({ value: '', error: '' })
  const [cidade, setCidade] = useState({ value: [], error: '' })
  const [convenio, setConvenio] = useState({ value: [], error: '' })
  const [password, setPassword] = useState({ value: '', error: '' })
  const [loading, setLoading] = useState()

  const { uid } = firebase.auth().currentUser
  const [plantoes, setPlantoes] = useState([])

  return (
    <Background>
      <TopBar />
      <View style={styles.headerLista}>
        <Header>Meu perfil?</Header>
        <Paragraph>
          Aqui você pode altera o seu perfil, enviar sua foto, atualizar suas
          preferências e muito mais.
        </Paragraph>
      </View>
      <Button
        loading={loading}
        mode="contained"
        onPress={() => navigation.navigate('EditarPerfil')}
        style={{ marginTop: 24, backgroundColor: '#fd7304' }}
      >
        Atualizar meu perfil
      </Button>
    </Background>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
})
