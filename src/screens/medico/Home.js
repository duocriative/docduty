import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text, Modal } from 'react-native'
import moment from 'moment'
import { AntDesign } from '@expo/vector-icons';
import DropDown from '../../components/DropDown'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TopBar from '../../components/TopBar'
import Background from '../../components/Background'
import SearchInput from '../../components/SearchInput'
import { estados } from '../../helpers/estados-cidades.json'
import SelectFilter from '../../components/SelectFilter'
import CardPlantaoNovo from '../../components/CardPlantaoNovo'
import { firebase, db } from '../../firebase/config'
import {Timestamp} from 'firebase/firestore'
import { fetchApiData } from '../../api/api'
import MetaInfoIcon from '../../components/MetaInfoIcon'

import { theme } from '../../core/theme'
import { State } from 'react-native-gesture-handler'

import PushNotifications from '../../components/PushNotifications'

export default function Home({ navigation }) {
  const { uid, phone } = firebase.auth().currentUser
  const [plantoes, setPlantoes] = useState([])
  const [arrayHolder, setHolderArr] = useState([])
  const [searchQuery, setSearchQuery] = React.useState('')
  const [modalVisible, setModalVisible] = useState(false)
  const [cidade, setCidade] = useState({ value: [], error: '' })
  const [especialidade, setEspecialidade] = useState({ value: [], error: '' })
  const [hospital, setHospital] = useState({ value: [], error: '' })

  const initalState = {
    //especialidade: { value: false, error: '' },
    //hospital: { value: false, error: '' },
  }

  const [state, setState] = useState(initalState)

  async function fetchEspecialidadesJSON() {
    const response = await fetch('https://docduty.com.br/aplicativo/wp-json/wp/v2/especialidades?_embed');
    const esp = await response.json();
    const arr = []
    esp.forEach(el => {
      const { especialidade, cor } = el
      arr.push({
        especialidade: el.title.rendered,
        cor: el.acf.cor
      })
    })
    return arr
  }

  const especialidades = [
    {
      name: 'Especialidades',
      id: 0,
      children: [
        { label: 'Cardiologia', id: 'cardiologia' },
        { label: 'Cirurgia Geral', id: 'cirurgia geral' },
        { label: 'Clinica Médica', id: 'clinica medica' },
        { label: 'Dermatologia', id: 'dermatologia' },
        { label: 'Endocrinologia', id: 'endocrinologia' },
        { label: 'Neurologia', id: 'neurologia' },
        { label: 'Oncologia', id: 'oncologia' },
        { label: 'Oftalmologia', id: 'oftalmologia' },
        { label: 'Ortopedia', id: 'ortopedia' },
        { label: 'Pediatria', id: 'pediatria' },
        { label: 'Urologia', id: 'urologia' },
      ]
    }
  ]

  const hospitais = [
    {
      name: 'Hospitais',
      id: 0,
      children: [
        { name: 'Hospital São Lucas', id: 'Hospital São Lucas' },
        { name: 'Hospital Das Clínicas', id: 'Hospital Das Clínicas' },
      ]
    }
  ]

  function getStartOfToday() {
    const now = new Date()
    now.setHours(20)
    //now.setHours(20, 0, 0, 0) // +5 hours for Eastern Time
    //const locale = now.toLocaleString('pt-BR', { timeZone: 'America/Sao_Paulo' })
    const timestamp = firebase.firestore.Timestamp.fromDate(now)
    
    return timestamp // ex. 1631246400
  }

  useEffect(() => {
    fetchEspecialidadesJSON().then(vl => {
      db.collection('plantoes')
      .where('data', '>', getStartOfToday())
      .onSnapshot((querySnapshot) => {
        const lista = []
        querySnapshot.forEach((snapshot) => {
          const { titulo, data, status, hora, local, cidade, hospital, tipo, sexo, especialidade, cor, user_name, user_id } =
            snapshot.data()

            function isEspecialidade(value) {
              return value.especialidade == especialidade
            }
            let especialidades = vl
            const filtered = especialidades.filter(isEspecialidade)

          if(status != 'Aceito'){
            lista.push({
              id: snapshot.id,
              titulo,
              dia: moment(data.toDate()).format('DD/MM'),
              hora,
              cor: filtered[0].cor,
              local,
              cidade: cidade[0],
              hospital,
              tipo,
              sexo,
              especialidade,
              user_name,
            })
          }
        })
        setPlantoes(lista)
        setHolderArr(lista)
      })
    });
  }, [])

  useEffect(() => {
    getStartOfToday()
  })

  const onChangeSearch = searchQuery => {
    const newData = arrayHolder.filter(item => {
      const itemData = item.titulo
      return itemData.indexOf(searchQuery) > -1
    })
    setPlantoes(newData)
    setSearchQuery(searchQuery)
  };

  const onFilterSearch = () => {
    if(especialidade.length == 0 && hospital.length == 0 && cidade.length == 0) {
      setPlantoes(plantoes)
      setModalVisible(false)
    }else{
      const newData = arrayHolder.filter(item => {
        const itemEspecialidade = especialidade.value
        const itemHospital = hospital.value
        const itemCidade = cidade.value

        let filterAdd = true

        if(itemEspecialidade.length !== 0){
          filterAdd = filterAdd && item.especialidade == itemEspecialidade
        }
        if(itemHospital.length !== 0){
          filterAdd = filterAdd && item.hospital == itemHospital
        }
        if(itemCidade.length !== 0 ){
          filterAdd = filterAdd && item.cidade == itemCidade
        }
        return filterAdd
      })
      setPlantoes(newData)
      setModalVisible(false)
    }
  };

  const onSelectedCidadesChange = (selectedItems) => {
    setCidade({ value: selectedItems })
  }

  const onSelectedEspecialidadeChange = (selectedItems) => {
    setEspecialidade({ value: selectedItems })
  }

  const onSelectedHospitalChange = (selectedItems) => {
    setHospital({ value: selectedItems })
  }

  return (
    <Background>
      <TopBar />

      <Modal
        animationType="fade"
        visible={modalVisible}
        transparent={true}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalFilter}>
            <Text onPress={() => setModalVisible(false)}>
              <AntDesign name="closecircle" size={24} color="#fd7304" />
            </Text>
            <Header>Escolha um filtro abaixo</Header>

            <SelectFilter
              labelOut="Especialidade"
              items={especialidades}
              onSelectedItemsChange={onSelectedEspecialidadeChange}
              selectText="Selecione uma especialidade"
              showDropDowns={true}
              expandDropDowns={true}
              showChips={false}
              readOnlyHeadings={true}
              single={true}
              selectedItems={especialidade.value}
            />

            <SelectFilter
              labelOut="Hospital"
              items={hospitais}
              onSelectedItemsChange={onSelectedHospitalChange}
              selectText="Selecione um hospital"
              showDropDowns={true}
              expandDropDowns={true}
              showChips={false}
              readOnlyHeadings={true}
              single={true}
              selectedItems={hospital.value}
            />

            <SelectFilter
              labelOut="Cidade"
              items={estados}
              onSelectedItemsChange={onSelectedCidadesChange}
              selectText="Selecione uma cidade"
              showDropDowns={true}
              showChips={false}
              readOnlyHeadings={true}
              single
              selectedItems={cidade.value}
            />
            <Button
            mode="contained"
            style={{ marginTop: 24, backgroundColor: '#fd7304' }}
            onPress={onFilterSearch}
            >
            Filtrar
          </Button>

          <Button
            mode="contained"
            style={{ marginTop: 6, color: theme.colors.primary,  backgroundColor: '#ccc' }}
            onPress={() => {
              setCidade({value: []})
              setEspecialidade({value: []})
              setHospital({value: []})
            }}
            >
            Limpar
          </Button>
          </View>
        </View>
      </Modal>

      <PushNotifications/>

      <SearchInput
        placeholder="Pocure aqui seu plantão"
        autoCapitalize='none'
        onChangeText={searchQuery => onChangeSearch(searchQuery)}
        value={searchQuery}
      />
      <View style={styles.headerLista}>
        <Header>Plantões Disponíveis</Header>
        <Text style={styles.filterButton} onPress={() => setModalVisible(true)}>Filtrar</Text>
      </View>
      <View>
        {plantoes.length == 0 && (
          <View>
            <Text>Nenhum resultado encontrado para sua busca</Text>
          </View>
        )}
        {plantoes.map((plantao) => (
          <CardPlantaoNovo
            key={plantao.id}
            title={plantao.titulo}
            local={plantao.local}
            tipo={plantao.tipo}
            button={plantao.especialidade}
            dia={plantao.dia}
            hora={plantao.hora}
            anunciante={plantao.user_name}
            cor={plantao.cor}
            sexo={plantao.sexo}
            onPress={() => {
              navigation.navigate('DetalhesPlantao', {
                itemId: plantao.id,
                cor: plantao.cor
              })
            }}
          />
        ))}
        {/* <CardPlantaoNovo
          title="Plantão"
          button="Ortopedia"
          dia="27/02"
          hora="19h às 7h"
          local="Hospital Alberto Torres - Niterói (RJ)"
          anunciante="Dra. Nathália Miranda"
        />*/}
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  headerLista: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subtitulo: {
    fontWeight: 'bold',
    fontSize: 36,
    color: theme.colors.text1,
  },
  filterButton: {
    color: theme.colors.primary,
    textDecorationLine: 'underline'
  },
  modalContainer: {
    //backgroundColor: '#000000AA',
    flex: 1,
    alignItems: "center",
    justifyContent: 'flex-end',
    marginTop: 48,
  },
  modalFilter: {
    backgroundColor: '#fff',
    padding: 16,
    height: '79%',
    width: '100%'
  }
})
