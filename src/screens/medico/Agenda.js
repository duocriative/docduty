import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import moment from 'moment'
import { Calendar, LocaleConfig } from 'react-native-calendars'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TopBar from '../../components/TopBar'
import Background from '../../components/Background'
import { firebase, db } from '../../firebase/config'

import { theme } from '../../core/theme'

LocaleConfig.locales.ptBr = {
  monthNames: [
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro',
  ],
  monthNamesShort: [
    'Jan.',
    'Fev.',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul.',
    'Ago',
    'Set.',
    'Out.',
    'Nov.',
    'Dez.',
  ],
  dayNames: [
    'Domingo',
    'Segunda',
    'Terça',
    'Quarta',
    'Quinta',
    'Sexta',
    'Sábado',
  ],
  dayNamesShort: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
}

LocaleConfig.defaultLocale = 'ptBr'

export default function Agenda({ navigation }) {
  const { uid } = firebase.auth().currentUser
  const [plantoes, setPlantoes] = useState(["2021-04-26"])
  const [userid, setId] = useState()

  useEffect(() => {
    db.collection('plantoes')
      .where('user_rel', '==', uid)
      .onSnapshot((querySnapshot) => {
        const lista = []
        querySnapshot.forEach((snapshot) => {
          const { data, status } =
            snapshot.data()
          if(status == 'Aceito'){
            lista.push({
              id: snapshot.id,
              dia: moment(data.toDate()).format('Y-MM-DD'),
            })
          }
        })
        var arr = []
        lista.map((item) => {
          //arr.push("'"+item.dia+"'" + ': {selected: true, marked: true},')
          arr.push(item.dia)
        })

        let mark = {}

        arr.forEach(day => {
          mark[day] = {
            marked: false,
            selected: true
          }
        })

        setPlantoes(mark)
        console.log(plantoes)

      })
  }, [])

  return (
    <Background>
      <TopBar />
      <Header>Minha agenda</Header>
      <Calendar
        /*onDayPress={(plantoes) => {
          navigation.navigate('Consultas')
        }}*/
        markedDates={ plantoes }
        monthFormat="MMMM yyyy"
        // Specify style for calendar container element. Default = {}
        style={{
          padding: 0,
          backgroundColor: theme.colors.tint,
        }}
        // Specify theme properties to override specific styles for calendar parts. Default = {}
        theme={{
          arrowColor: 'white',
          textDayFontSize: 14,
          selectedDayBackgroundColor: '#fd7304',
          dayTextColor: '#000',
          todayTextColor: '#fd7304',
          'stylesheet.calendar.main': {
            monthView: {
              backgroundColor: 'white',
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
            },
          },
          'stylesheet.calendar.header': {
            header: {
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingLeft: 10,
              paddingRight: 10,
              marginTop: 6,
              marginBottom: 6,
              alignItems: 'center',
              backgroundColor: '#0472fd',
              borderRadius: 10,
            },
            headerContainer: {
              flexDirection: 'row',
            },
            monthText: {
              fontSize: 24,
              fontWeight: 'bold',
              color: 'white',
              margin: 10,
            },
            week: {
              marginTop: 7,
              flexDirection: 'row',
              justifyContent: 'space-around',
              backgroundColor: 'white',
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
            },
            dayHeader: {
              marginTop: 2,
              marginBottom: 7,
              width: 32,
              fontSize: 18,
              fontWeight: 'bold',
              textAlign: 'center',
              color: '#0472fd',
            },
          },
        }}
      />
    </Background>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subtitulo: {
    fontWeight: 'bold',
    fontSize: 36,
    color: theme.colors.text1,
  },
})
