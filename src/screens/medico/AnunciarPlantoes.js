import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import moment from 'moment'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TopBar from '../../components/TopBar'
import Background from '../../components/Background'
import CardPlantao from '../../components/CardPlantao'
import { firebase, db } from '../../firebase/config'

import { theme } from '../../core/theme'

export default function AnunciarPlantoes({ navigation }) {
  const { uid } = firebase.auth().currentUser
  const [plantoes, setPlantoes] = useState([])

  async function fetchEspecialidadesJSON() {
    const response = await fetch('https://docduty.com.br/aplicativo/wp-json/wp/v2/especialidades?_embed');
    const esp = await response.json();
    const arr = []
    esp.forEach(el => {
      const { especialidade, cor } = el
      arr.push({
        especialidade: el.title.rendered,
        cor: el.acf.cor
      })
    })
    return arr
  }

  useEffect(() => {
    fetchEspecialidadesJSON().then(vl => {
      db.collection('plantoes')
      .where('user_id', '==', uid)
      .onSnapshot((querySnapshot) => {
        const lista = []
        querySnapshot.forEach((snapshot) => {
          const { titulo, data, hora, cor, status, especialidade, user_rel } =
            snapshot.data()

            function isEspecialidade(value) {
              return value.especialidade == especialidade
            }
            let especialidades = vl
            const filtered = especialidades.filter(isEspecialidade)
          
          const dia = moment(data.toDate()).format('DD/MM')
          const statusAnuncio =
            status === 'Pedido em analise' ? 'Aguardando resposta' : status
          lista.push({
            id: snapshot.id,
            titulo,
            dia,
            cor: filtered[0].cor,
            hora,
            status: statusAnuncio,
            especialidade,
            user_rel,
          })
        })
        setPlantoes(lista)
      })
    });
  }, [])
  return (
    <Background>
      <TopBar />

      <Header>Anunciar Plantões</Header>
      <Button
        mode="contained"
        onPress={() => navigation.navigate('CriarPlantao')}
        style={{ width: 224, backgroundColor: '#fd7304' }}
      >
        Criar um anúncio agora
      </Button>
      <Header>Meus anúncios</Header>
      <View>
        {plantoes.map((plantao) => (
          <CardPlantao
            key={plantao.id}
            title={plantao.titulo}
            button={plantao.especialidade}
            cor={plantao.cor}
            dia={plantao.dia}
            hora={plantao.hora}
            status={plantao.status}
            onPress={() => {
              navigation.navigate('DetalhesPlantaoAnuncio', {
                itemId: plantao.id,
                userRel: plantao.user_rel,
                cor: plantao.cor
              })
            }}
          />
        ))}
        {/*<CardPlantao
          title="Plantão"
          button="Ortopedia"
          dia="27/02"
          hora="19h às 7h"
          status="Pedido em analise"
        />*/}
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  headerLista: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subtitulo: {
    fontWeight: 'bold',
    fontSize: 36,
    color: theme.colors.text1,
  },
})
