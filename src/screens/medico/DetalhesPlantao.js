import React, { useState, useEffect } from 'react'
import {
  Linking,
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native'
import moment from 'moment'
import { Card, Title, Paragraph, Subheading } from 'react-native-paper'
import Header from '../../components/Header'
import Button from '../../components/Button'
import TopBar from '../../components/TopBar'
import Background from '../../components/Background'
import MetaInfoIcon from '../../components/MetaInfoIcon'
import TextInput from '../../components/TextInput'
import CardStatus from '../../components/CardStatus'
import { Stethoscope } from '../../assets/icons'
import { firebase, db } from '../../firebase/config'
import { block } from 'react-native-reanimated'
import { AutoFocus } from 'expo-camera/build/Camera.types'

export default function DetalhesPlantao({ route, navigation }) {
  const { uid } = firebase.auth().currentUser
  const [loading, setLoading] = useState(true)
  const [plantao, setPlantao] = useState()
  const { itemId, cor } = route.params

  const getPlantaoById = async (id) => {
    const dbRef = db.collection('plantoes').doc(id)
    const doc = await dbRef.get()
    const plantaoData = doc.data()
    plantaoData.data = moment(plantaoData.data.toDate()).format('DD/MM')
    setPlantao({ ...plantaoData, id: doc.id })
    setLoading(false)
  }

  const updatePlantao = async () => {
    const userRef = db.collection('plantoes').doc(itemId)
    // console.log(userRef)
    await userRef.update({
        user_rel: uid,
        status: 'Pedido em analise',
      })
      .then((retorno) => {
        alert('Solicitação enviada')
        navigation.navigate('Home')
      })
      .catch((error) => {
        alert(error)
      })
    // props.navigation.navigate("UsersList");
  }

  const openWhatsapp = (value) => {
    Linking.canOpenURL("whatsapp://send").then(supported => {
      if (supported) {
        return Linking.openURL(
          "whatsapp://send?phone=+55"+value
        );
      } else {
        return Linking.openURL(
          "https://api.whatsapp.com/send?phone=+55"+value
        );
      }
    })
  }

  useEffect(() => {
    getPlantaoById(itemId)
  }, [])
  // const user = getUserData()
  if (loading) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size="large" color="#9E9E9E" />
      </View>
    )
  }
  return (
    <Background>
      <TopBar />
      <View style={styles.cardHead}>
        <View style={styles.viewTitle}>
          <Title style={styles.cardTitle}>
            {plantao.titulo} {plantao.tipo}
          </Title>
        </View>
        <TouchableOpacity style={{ height: 27, width: 125, borderRadius: 27, backgroundColor: cor, display: 'flex', marginBottom: 0, alignItems: 'center', justifyContent: 'center', alignSelf: 'center', }}>
          <Text style={styles.buttonText}>{plantao.especialidade}</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Card style={{ paddingTop: 14, backgroundColor: cor, borderRadius: 14, marginVertical: 11, }}>
          <Card.Content style={styles.cardContent}>
            <MetaInfoIcon text={plantao.local} icon="MapMarkerOutline" />
            <View style={styles.cardMetaContent}>
              <MetaInfoIcon text={plantao.data} icon="CalendarMonthOutline" />
              <MetaInfoIcon text={plantao.hora} icon="AvTimer" />
            </View>
            <View style={styles.cardMetaContent}>
              <MetaInfoIcon
                text={'Dr. ' + plantao.user_name}
                icon="Stethoscope"
              />
            </View>
            { plantao.verWhatsapp != false &&
              <TouchableOpacity style={styles.cardMetaContent} onPress={() => openWhatsapp(plantao.telefone)}>
                <MetaInfoIcon
                text={plantao.telefone}
                icon="Whatsapp"
                fillIcon="#117d06"
              />
              </TouchableOpacity>
            }
            <View style={styles.consultaDetalhesContent}>
              <Subheading
                style={{ fontSize: 18, color: '#000', fontWeight: 'bold' }}
              >
                Detalhe do Plantão
              </Subheading>
              <Paragraph>{plantao.obs}</Paragraph>
            </View>

            <View style={{ marginBottom: 12, marginTop: 12 }}>
                {plantao.plantonista > 0 && 
                  <Text>Número de plantonistas: {plantao.plantonista}</Text>
                }
                {plantao.cti === 'sim' && 
                  <Text>CTI: Sim</Text>
                }
                {plantao.plantaoCirurgia === 'sim' && 
                  <Text>Plantão cirurgico: Sim</Text>
                }
            </View>

            <View style={styles.consultaTags}>
              <Subheading
                style={{ fontSize: 18, color: '#000', fontWeight: 'bold' }}
              >
                Características do Plantão
              </Subheading>
              <View style={styles.tagsContainer}>
                { plantao.tags.map(tags => 
                  <View style={styles.tag}>
                    <Text>{tags}</Text>
                  </View>
                ) }
              </View>
            </View>

          </Card.Content>
        </Card>
      </View>
      <View>
        {
          plantao.user_rel && plantao.status == 'Aceito' &&
          <View>
            <Button
              icon={({ size, color }) => <Stethoscope fill="#fff" />}
              mode="contained"
              onPress={() => updatePlantao()}
              style={{ marginTop: 24, backgroundColor: '#fd7304' }}
            >
              Navegar com waze
            </Button>
          </View>
        }
        {
          plantao.status == 'aberto' &&
          <Button
            icon={({ size, color }) => <Stethoscope fill="#fff" />}
            mode="contained"
            onPress={() => updatePlantao()}
            style={{ marginTop: 24, backgroundColor: '#fd7304' }}
          >
            Aceitar esse Plantão
          </Button>
        }

        {
          plantao.status == 'Aceito' &&
          <CardStatus status={plantao.status}></CardStatus>
        }
        {
          plantao.status == 'Pedido em analise' &&
          <View>
            <Button
              mode="contained"
              style={{ marginTop: 24, marginLeft: 'auto', marginRight: 'auto', width: '74%', backgroundColor: '#fd7304' }}
            >
              Pedido em analise...
            </Button>
          </View>
        }
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  card: {
    paddingTop: 14,
    backgroundColor: '#0472fd',
    borderRadius: 14,
    marginVertical: 11,
  },
  viewTitle: {
    width: '60%'
  },
  cardHead: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
    marginBottom: 10,
  },
  cardTitle: {
    fontWeight: 'bold',
    lineHeight: 24,
    marginRight: 24,
    marginTop: 20,
    textTransform: 'capitalize'
  },
  cardContent: {
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    paddingLeft: 10,
    backgroundColor: '#fff',
    minHeight: 106,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  cardMetaContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    maxWidth: '90%',
    paddingTop: 10,
  },
  consultaDetalhesContent: {
    borderTopWidth: 1,
    borderTopColor: '#0472fd',
    marginVertical: 20,
    paddingVertical: 20,
    width: '100%',
  },
  consultaDetalhesResposta: {
    backgroundColor: '#000615',
    color: '#fff',
    width: '100vw',
    alignSelf: 'stretch',
  },
  consultaDetalhesRespostaHeading: {
    color: '#43c0ba',
    fontWeight: 'bold',
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 125,
    height: 27,
    borderRadius: 27,
    backgroundColor: '#fd7e02',
    display: 'flex',
    marginBottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 12,
    color: '#fff',
  },
  tagsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  tag: {
    backgroundColor: '#e2e2e2',
    borderRadius: 8,
    margin: 12,
    paddingTop: 12,
    paddingBottom: 12,
    paddingRight: 24,
    paddingLeft: 24
  }
})
