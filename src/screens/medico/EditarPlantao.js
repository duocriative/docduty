import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native'
import { Checkbox } from 'react-native-paper'
import DropDown from '../../components/DropDown'
import { firebase, db } from '../../firebase/config'
import DatePicker from '../../components/DatePicker'
import Background from '../../components/Background'
import Header from '../../components/Header'
import TopBar from '../../components/TopBar'
import Button from '../../components/Button'
import TextInput from '../../components/TextInput'
import SwitchButton from '../../components/SwitchButton'
//import TagsInput from '../../components/TagsInput'
import { Tags } from '../../helpers/tags.json'
import TagInput from 'react-native-tags-input';
import { fetchApiData } from '../../api/api'
import { theme } from '../../core/theme'

import Toast from '../../components/Toast'
import { render } from 'react-dom'

export default function EditarPlantao({ navigation }) {
  const { uid, displayName } = firebase.auth().currentUser
  const [perfil, setPerfil] = useState([])
  const [checked, setChecked] = React.useState(false)

  const initalState = {
    user_id: uid,
    user_name: displayName,
    titulo: { value: '', error: '' },
    local: { value: '', error: '' },
    especialidade: { value: null, error: '' },
    tipo: { value: null, error: '' },
    valor: { value: '', error: '' },
    data: { value: new Date(), error: '' },
    hora: { value: '', error: '' },
    verCrm: { value: false, error: '' },
    hospital: { value: false, error: '' },
    plantonista: { value: null, error: '' },
    cti: { value: null, error: '' },
    plantaoCirurgia: { value: null, error: '' },
    sala: { value: null, error: '' },
    verWhatsapp: { value: false, error: '' },
    obs: { value: '', error: '' },
    status: { value: 'aberto' },
    views: { value: 0 },
    rating: { value: 0 },
    user_rel: { value: null },
    tags: {
      tag: '',
      tagsArray: []
    },
    show: true
  }

  const [state, setState] = useState(initalState)
  const [listaespecialidades, setEspecialidades] = useState([])

  useEffect(() => {
    const route = `https://docduty.com.br/aplicativo/wp-json/wp/v2/especialidades?_embed`;
    fetchApiData(route).then(res => {
      res.forEach(el => {
        const esp = []
        esp.push({
          label: el.title.rendered,
          value: el.title.rendered 
          //cor: el.acf.cor
        })
        setEspecialidades(esp)
      });
    });
  }, [])

  useEffect(() => {
    db.collection('users')
      .doc(uid)
      .get()
      .then((item) => {
        //setPerfil(lista)
        setPerfil(item.data())
      })
  }, [])

  const handleChangeState = (value, name) => {
    if(name === 'data'){
      setState({ ...state, [name]: value, show: false })
    }else{
      setState({ ...state, [name]: value })
    }
  }

  const [loading, setLoading] = useState()
  const [error, setError] = useState()
  const [returnMsg, setReturnMsg] = useState()

  const tiposPlantao = [
    { label: 'Fixo', value: 'fixo' },
    { label: 'Coringa', value: 'coringa' },
  ]
  const especialidades = [
    { label: 'Cardiologia', value: 'cardiologia' },
    { label: 'Cirurgia Geral', value: 'cirurgia geral' },
    { label: 'Dermatologia', value: 'dermatologia' },
    { label: 'Endocrinologia', value: 'endocrinologia' },
    { label: 'Neurologia', value: 'neurologia' },
    { label: 'Ortopedia', value: 'ortopedia' },
    { label: 'Oncologia', value: 'oncologia' },
    { label: 'Pediatria', value: 'pediatria' },
    { label: 'Urologia', value: 'urologia' },
  ]

  const plantonistas = [
    { label: '1', value: '1' },
    { label: '2', value: '2' },
    { label: '3', value: '3' },
    { label: '4', value: '4' },
    { label: '5', value: '5' },
    { label: '6', value: '6' },
    { label: '7', value: '7' },
    { label: '8', value: '8' },
  ]

  const bool = [
    { label: 'Sim', value: 'sim' },
    { label: 'Não', value: 'não' }
  ]

  const hospitais = [
    { label: 'Hospital São Lucas', value: 'Hospital São Lucas' },
    { label: 'Hospital Das Clínicas', value: 'Hospital Das Clínicas' },
  ]

  const salvarForm = async () => {
    setLoading(true)
    try {
      await db.collection('plantoes').add({
        user_id: state.user_id,
        user_name: state.user_name,
        titulo: state.titulo.value,
        local: state.local.value,
        hospital: state.hospital.value,
        especialidade: state.especialidade.value,
        tipo: state.tipo.value,
        valor: state.valor.value,
        data: state.data.value,
        hora: state.hora.value,
        verCrm: state.verCrm.value,
        plantonista: state.plantonista.value,
        cti: state.cti.value,
        plantaoCirurgia: state.plantaoCirurgia.value,
        sala: state.sala.value,
        verWhatsapp: state.verWhatsapp.value,
        telefone: perfil.telefone,
        obs: state.obs.value,
        status: state.status.value,
        views: state.views.value,
        rating: state.rating.value,
        user_rel: state.user_rel.value,
        tags: state.tags
      })
      setLoading(false)
      // setReturnMsg('Plantão criado com sucesso')
      navigation.navigate("AnunciarPlantoes")
      alert('Plantão criado com sucesso')
      
      // props.navigation.navigate('UsersList')
    } catch (returnError) {
      console.log(returnError)
      setError(returnError.message)
    }
  }

  const renderTags = () => {
    function filterOptions(value) {
      return value.especialidade === state.especialidade.value
    }

    if(state.especialidade.value != null){
      var filtered = Tags.filter(filterOptions)
      if(filtered.length > 0){
        const listTags = filtered[0].tags.map((tag) =>
          <Checkbox.Item label={tag.name} 
          type
          mode="android"
          value={tag.name}
          />
        )
        return(
          <View>
              {listTags}
          </View>
        )
      }
    }
  }

  return (
    <Background>
      <TopBar />
      <Header>Editar plantão</Header>
      <TextInput
        labelOut="Título do plantão"
        returnKeyType="next"
        value={state.titulo.value}
        onChangeText={(val) => {
          handleChangeState({ value: val, error: '' }, 'titulo')
        }}
        error={!!state.titulo.error}
        errorText={state.titulo.error}
      />
      <TextInput
        labelOut="Local"
        returnKeyType="next"
        value={state.local.value}
        onChangeText={(val) => {
          handleChangeState({ value: val, error: '' }, 'local')
        }}
        error={!!state.local.error}
        errorText={state.local.error}
      />
      <DropDown
        labelOut="Hospital"
        valor={state.hospital.value}
        itens={hospitais}
        placeholder=""
        onChangeValue={(val) => {
          handleChangeState({ value: val, error: '' }, 'hospital')
        }}
        error={!!state.especialidade.error}
        errorText={state.especialidade.error}
      />
      <DropDown
        labelOut="especialidade"
        valor={state.especialidade.value}
        itens={especialidades}
        placeholder=""
        onChangeValue={(val) => {
          handleChangeState({ value: val, error: '' }, 'especialidade')
        }}
        error={!!state.especialidade.error}
        errorText={state.especialidade.error}
      />
      <DropDown
        labelOut="Tipo (Fixo ou Coringa)"
        valor={state.tipo.value}
        itens={tiposPlantao}
        placeholder=""
        onChangeValue={(val) => {
          handleChangeState({ value: val, error: '' }, 'tipo')
        }}
        error={!!state.tipo.error}
        errorText={state.tipo.error}
      />
      <DropDown
        labelOut="Número de plantonistas"
        valor={state.plantonista.value}
        itens={plantonistas}
        placeholder=""
        onChangeValue={(val) => {
          handleChangeState({ value: val, error: '' }, 'plantonista')
        }}
        error={!!state.plantonista.error}
        errorText={state.plantonista.error}
      />
      <TextInput
        labelOut="Valor"
        returnKeyType="next"
        value={state.valor.value}
        onChangeText={(val) => {
          handleChangeState({ value: val, error: '' }, 'valor')
        }}
        error={!!state.valor.error}
        errorText={state.valor.error}
      />
      { state.show && (
        <DatePicker
        labelOut="Data"
        testID="data"
        value={state.data.value}
        mode="date"
        // is24Hour={true}
        display="default"
        onChange={(event, val) => {
          handleChangeState({ value: val, error: '' }, 'data')
        }}
        error={!!state.data.error}
        errorText={state.data.error}
      />
      ) }
      <TextInput
        labelOut="Hora"
        returnKeyType="next"
        value={state.hora.value}
        onChangeText={(val) => {
          handleChangeState({ value: val, error: '' }, 'hora')
        }}
        error={!!state.hora.error}
        errorText={state.hora.error}
      />

      { state.especialidade.value === "pediatria" && (
        <DropDown
          labelOut="Suporte de CTI"
          valor={state.cti.value}
          itens={bool}
          placeholder=""
          onChangeValue={(val) => {
            handleChangeState({ value: val, error: '' }, 'cti')
          }}
          error={!!state.cti.error}
          errorText={state.cti.error}
        />
      )}

      { state.especialidade.value === "clínica médica" && (
        <DropDown
          labelOut="Suporte de CTI"
          valor={state.cti.value}
          itens={bool}
          placeholder=""
          onChangeValue={(val) => {
            handleChangeState({ value: val, error: '' }, 'cti')
          }}
          error={!!state.cti.error}
          errorText={state.cti.error}
        />
      )}

      {state.especialidade.value == 'clínica médica' && (
        <DropDown
          labelOut="Sala"
          valor={state.sala.value}
          itens={bool}
          placeholder=""
          onChangeValue={(val) => {
            handleChangeState({ value: val, error: '' }, 'sala')
          }}
          error={!!state.sala.error}
          errorText={state.sala.error}
        />
      )}

      {state.especialidade.value == 'oftalmologia' && (
        <DropDown
          labelOut="Plantão Cirurgico"
          valor={state.plantaoCirurgia.value}
          itens={bool}
          placeholder=""
          onChangeValue={(val) => {
            handleChangeState({ value: val, error: '' }, 'plantaoCirurgia')
          }}
          error={!!state.plantaoCirurgia.error}
          errorText={state.plantaoCirurgia.error}
        />
      )}

      {renderTags()}

      <SwitchButton
        labelOut="Divulgar meu CRM"
        value={state.verCrm.value}
        onValueChange={() => {
          handleChangeState({ value: !state.verCrm.value, error: '' }, 'verCrm')
        }}
        error={!!state.verCrm.error}
        errorText={state.verCrm.error}
      />

      <SwitchButton
        labelOut="Divulgar meu Whatsapp"
        value={state.verWhatsapp.value}
        onValueChange={() => {
          handleChangeState(
            { value: !state.verWhatsapp.value, error: '' },
            'verWhatsapp'
          )
        }}
        error={!!state.verWhatsapp.error}
        errorText={state.verWhatsapp.error}
      />

      <View style={styles.tagsContainer}>
      </View>

      <TextInput
        labelOut="Observações"
        returnKeyType="next"
        multiline
        value={state.obs.value}
        onChangeText={(val) => {
          handleChangeState({ value: val, error: '' }, 'obs')
        }}
        error={!!state.obs.error}
        errorText={state.obs.error}
      />

      <Button
        loading={loading}
        mode="contained"
        onPress={salvarForm}
        style={{ marginTop: 24, backgroundColor: '#fd7304' }}
      >
        Cadastrar Plantão
      </Button>

      <Toast message={error} onDismiss={() => setError('')} />
    </Background>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  tagsInput: {
    backgroundColor: '#fff',
    borderColor: '#8D8D8D',
    borderRadius: 4,
    borderWidth: 1,
    marginLeft: 0,
    padding: 6,
    width: '100%'
  }
})
