// Médicos
export { default as Home } from './Home'
export { default as Plantoes } from './Plantoes'
export { default as Anuncios } from './Anuncios'
export { default as AnunciarPlantoes } from './AnunciarPlantoes'
export { default as CriarPlantao } from './CriarPlantao'
export { default as EditarPlantao } from './EditarPlantao'
export { default as DetalhesPlantao } from './DetalhesPlantao'
export { default as DetalhesPlantaoAnuncio } from './DetalhesPlantaoAnuncio'

export { default as Consulta } from './Consulta'
export { default as Consultas } from './Consultas'
export { default as DetalhesConsulta } from './DetalhesConsulta'
export { default as Agenda } from './Agenda'

export { default as Perfil } from './Perfil'
export { default as EditarPerfil } from './EditarPerfil'
