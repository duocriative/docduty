import React, { useState, useEffect } from 'react'
import {
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from 'react-native'
import Background from '../components/Background'
import Header from '../components/Header'
import Button from '../components/Button'
import TextInput from '../components/TextInput'
import { estados } from '../helpers/estados-cidades.json'
import { convenios } from '../helpers/convenios.json'
import SelectFilter from '../components/SelectFilter'
import { theme } from '../core/theme'

import { logoutUser, getUserData } from '../api/auth-api'
import Toast from '../components/Toast'

export default function ProfileScreen({ navigation }) {
  const [name, setName] = useState({ value: '', error: '' })
  const [email, setEmail] = useState({ value: '', error: '' })
  const [telefone, setTelefone] = useState({ value: '', error: '' })
  const [cidade, setCidade] = useState({ value: [], error: '' })
  const [convenio, setConvenio] = useState({ value: [], error: '' })
  const [password, setPassword] = useState({ value: '', error: '' })
  const [loading, setLoading] = useState()
  const [error, setError] = useState()

  const user = getUserData()
  console.log(user)

  const onSelectedCidadesChange = (selectedItems) => {
    setCidade({ value: selectedItems })
  }

  const onSelectedConveniosChange = (selectedItems) => {
    setConvenio({ value: selectedItems })
  }


  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Background>
          <Header>
            Preencha os dados abaixo para editar o seu perfil no Doc Duty
          </Header>
          <Button mode="contained" onPress={logoutUser} style={{ width: 120 }}>
          Logout
        </Button>
          <TextInput
            labelOut="Nome"
            returnKeyType="next"
            value={name.value}
            onChangeText={(text) => setName({ value: text, error: '' })}
            error={!!name.error}
            errorText={name.error}
          />
          <TextInput
            labelOut="E-mail"
            returnKeyType="next"
            value={email.value}
            onChangeText={(text) => setEmail({ value: text, error: '' })}
            error={!!email.error}
            errorText={email.error}
            autoCapitalize="none"
            autoCompleteType="email"
            textContentType="emailAddress"
            keyboardType="email-address"
          />
          <TextInput
            labelOut="Telefone"
            placeholder="(99) 99999-9999"
            returnKeyType="next"
            value={telefone.value}
            onChangeText={(text) => setTelefone({ value: text, error: '' })}
            error={!!telefone.error}
            errorText={telefone.error}
            autoCapitalize="none"
            autoCompleteType="tel"
            keyboardType="phone-pad"
            textContentType="telephoneNumber"
          />
          <SelectFilter
            labelOut="Convênio"
            items={convenios}
            onSelectedItemsChange={onSelectedConveniosChange}
            selectText=""
            showChips={false}
            single
            selectedItems={convenio.value}
            readOnlyHeadings
            showDropDowns={false}
          />
          <SelectFilter
            labelOut="Cidade"
            items={estados}
            onSelectedItemsChange={onSelectedCidadesChange}
            selectText=""
            showDropDowns
            showChips={false}
            readOnlyHeadings
            single
            selectedItems={cidade.value}
          />
          <TextInput
            labelOut="Senha de acesso"
            returnKeyType="done"
            value={password.value}
            onChangeText={(text) => setPassword({ value: text, error: '' })}
            error={!!password.error}
            errorText={password.error}
            secureTextEntry
          />
          <Button
            loading={loading}
            mode="contained"
            // onPress={onSignUpPressed}
            style={{ marginTop: 24, backgroundColor: '#fd7304' }}
          >
            Atualizar meu cadastro
          </Button>

          <Toast message={error} onDismiss={() => setError('')} />
        </Background>
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
})
