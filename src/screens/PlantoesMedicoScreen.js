import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import Header from '../components/Header'
import Button from '../components/Button'
import TopBar from '../components/TopBar'
import Background from '../components/Background'
import CardPlantao from '../components/CardPlantao'

import { theme } from '../core/theme'

export default function ConsultasPacienteScreen({ navigation }) {
  return (
    <Background>
      <TopBar />
      <View style={styles.headerLista}>
        <Header>Meus Plantões</Header>
        <Button
          mode="contained"
          onPress={() => navigation.navigate('AgendaPacienteScreen')}
          style={{ width: 224, backgroundColor: '#fd7304' }}
        >
          + Anunciar Plantão
        </Button>
      </View>
      <View>
        <CardPlantao
          title="Plantão"
          button="Ortopedia"
          dia="27/02"
          hora="19h às 7h"
          status="Pedido em analise"
        />
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  headerLista: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subtitulo: {
    fontWeight: 'bold',
    fontSize: 36,
    color: theme.colors.text1,
  },
})
