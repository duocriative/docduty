import React, { useState } from 'react'
import {
  ImageBackground,
  StyleSheet,
  View,
  KeyboardAvoidingView,
} from 'react-native'
import { Text } from 'react-native-paper'
import Logo from '../components/Logo'
import { theme } from '../core/theme'
import TextInput from '../components/TextInput'
import Button from '../components/Button'
import { emailValidator } from '../helpers/emailValidator'
import { sendEmailWithPassword } from '../api/auth-api'
import Toast from '../components/Toast'

export default function ResetPasswordScreen({ navigation }) {
  const [email, setEmail] = useState({ value: '', error: '' })
  const [loading, setLoading] = useState(false)
  const [toast, setToast] = useState({ value: '', type: '' })

  const sendResetPasswordEmail = async () => {
    const emailError = emailValidator(email.value)
    if (emailError) {
      setEmail({ ...email, error: emailError })
      return
    }
    setLoading(true)
    const response = await sendEmailWithPassword(email.value)
    if (response.error) {
      setToast({ type: 'error', message: response.error })
    } else {
      setToast({
        type: 'success',
        message: 'Email with password has been sent.',
      })
    }
    setLoading(false)
  }

  return (
    <View style={styles.background}>
      <ImageBackground
        source={require('../assets/background.png')}
        style={styles.image}
      >

        <KeyboardAvoidingView style={styles.container} behavior="padding">
          <Logo size="default" />
          <Text style={styles.header}>Vamos começar?</Text>
          <TextInput
            labelOut="Informe seu e-mail de cadastro"
            returnKeyType="done"
            value={email.value}
            onChangeText={(text) => setEmail({ value: text, error: '' })}
            error={!!email.error}
            errorText={email.error}
            autoCapitalize="none"
            autoCompleteType="email"
            textContentType="emailAddress"
            keyboardType="email-address"
            description="Você irá receber um link para redefinir a senha."
            style={styles.label}
          />
          <Button
            loading={loading}
            mode="contained"
            onPress={sendResetPasswordEmail}
            style={styles.resetButton}
          >
            Reenviar senha
          </Button>
          <Toast
            {...toast}
            onDismiss={() => setToast({ value: '', type: '' })}
          />
        </KeyboardAvoidingView>
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    backgroundColor: theme.colors.primary,
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
    justifyContent: 'center',
  },
  header: {
    color: '#fff',
    fontSize: 20,
    marginVertical: 25,
  },
  container: {
    flex: 1,
    padding: 20,
    width: '100%',
    maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  forgotPassword: {
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    margin: 0,
    fontSize: 13,
    color: '#fff',
    textDecorationLine: 'underline',
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  resetButton: {
    margin: 0,
    backgroundColor: '#fd7304',
  },
  label: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#fff',
  },
})
