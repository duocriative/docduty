// export { default as StartScreen } from './StartScreen'
export { default as LoginScreen } from './LoginScreen'
export { default as RegisterScreen } from './RegisterScreen'
export { default as RegisterMedico } from './RegisterMedico'
export { default as RegisterPaciente } from './RegisterPaciente'
export { default as ResetPasswordScreen } from './ResetPasswordScreen'
export { default as ProfileScreen } from './ProfileScreen'
export { default as HomeScreen } from './HomeScreen'
export { default as AuthLoadingScreen } from './AuthLoadingScreen'

// Pacientes

export { default as ConsultasPacienteScreen } from './ConsultasPacienteScreen'
export { default as AgendaPacienteScreen } from './AgendaPacienteScreen'

// Médicos
//export { default as HomeMedico } from './medico'
export { default as PlantoesMedicoScreen } from './PlantoesMedicoScreen'

export { default as ConsultaScreen } from './ConsultaScreen'
