import { DefaultTheme } from 'react-native-paper'

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#00154c',
    primary: '#00154c',
    tint: '#d9d9d9',
    secondary: '#43c0ba',
    text1: '#46bfb8',
    border: '#C9DDEC',
    error: '#f13a59',
    success: '#00B386',
    facebook: '#3b5998',
    google: '#2E7D32',
  },
}
