/* eslint-disable no-console */
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: "AIzaSyCEFvWYTVAyTp4Voq_fdLe-SrZjEflb42Y",
  authDomain: "doc-duty-279c8.firebaseapp.com",
  projectId: "doc-duty-279c8",
  storageBucket: "doc-duty-279c8.appspot.com",
  messagingSenderId: "697635314903",
  appId: "1:697635314903:web:4c9416b971c244ebf4a492",
  measurementId: "G-8GK9KHNDR4"
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const db = firebase.firestore()

export { firebase, db }
