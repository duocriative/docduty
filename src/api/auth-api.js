/* eslint-disable no-console */
import { useEffect, useState } from 'react'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

export const logoutUser = () => {
  firebase.auth().signOut()
}

export const signUpUser = async (data) => {
  try {
    const user = await firebase
      .auth()
      .createUserWithEmailAndPassword(data.email, data.password)
      .then((dataResponse) => {
        const uid = dataResponse.user.uid
        // your data here (dont forget to store the uid on the document)
        
        if(data.userType == 'medico'){
          //const imgPerfil = data.imgPerfil
          const userData = {
            user_id: uid,
            nome: data.nome,
            email: data.email,
            userType: data.userType,
            telefone: data.telefone,
            cidade: data.cidade,
            sexo: data.sexo,
            crm: data.crm,
          }

          //const uploadURI = data.imgPerfil
          //let filename = uploadURI.substring(uploadURI.lastIndexOf('/') + 1)
          //const upload = firebase.storage().ref(filename).putFile("file:///Users/daivison/Library/Developer/CoreSimulator/Devices/4300BA62-EF09-4BBA-9F91-50FA5C2734C1/data/Containers/Data/Application/5F72311E-4926-4743-B1C5-9B7ECF966CA8/Library/Caches/ExponentExperienceData/%2540duocriativ%252Fdoc-duty/ImagePicker/494355F7-F93D-4DCB-9706-975271CA317D.jpg")

          firebase.firestore().collection('users').doc(uid).set(userData)

          /*firebase.storage().ref(uid).putFile(imgPerfil).then((snapshot) => {
            console.log(`${imageName} has been successfully uploaded.`);
          })*/
          
        }else{
          const userData = {
            user_id: uid,
            nome: data.nome,
            email: data.email,
            userType: data.userType,
            telefone: data.telefone,
            convenio: data.convenio,
            cidade: data.cidade,
          }

          firebase.firestore().collection('users').doc(uid).set(userData)
        }

        //alert('Conta criada com sucesso')
      })
      .catch((error) => {
        if (error.code === 'auth/email-already-in-use') {
          alert('Esse e-mail já está em uso por outro usuário')
        } else {
          alert(error.message)
        }
        console.log(error)
      })

    firebase.auth().currentUser.updateProfile({
      displayName: data.nome
    })

    return { user }
  } catch (error) {
    return {
      error: error.message,
    }
  }
}

export const loginUser = async ({ email, password }) => {
  try {
    const user = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
    return { user }
  } catch (error) {
    return {
      error: error.message,
    }
  }
}

export const sendEmailWithPassword = async (email) => {
  try {
    await firebase.auth().sendPasswordResetEmail(email)
    return {}
  } catch (error) {
    return {
      error: error.message,
    }
  }
}

export const getUserInfo = async (uid) => {
  try {
    const userInfo = await firebase
      .firestore()
      .collection('users')
      .doc(uid)
      .get()
    return { userInfo }
  } catch (error) {
    return {
      error: error.message,
    }
  }
}

export const getUserData = () => {
  const [user, setUser] = useState()
  const { uid } = firebase.auth().currentUser

  const getUser = async () => {
    try {
      const documentSnapshot = await firebase
        .firestore()
        .collection('users')
        .doc(uid)
        .get()

      const userData = documentSnapshot.data()
      setUser(userData)
    } catch (error) {
      // do whatever
    }
  }

  // Get user on mount
  useEffect(() => {
    getUser()
  }, [])
  return user
}

const uploadImage = async (image, uid) => {
  const uri = image
  const childPath = `post/${uid}/${Math.random().toString(36)}`
  console.log(childPath)

  const response = await fetch(uri)
  const blob = await response.blob()

  const task = firebase.storage().ref().child(childPath).put(blob)

  const taskProgress = (snapshot) => {
    console.log(`transferred: ${snapshot.bytesTransferred}`)
  }

  const taskCompleted = () => {
    task.snapshot.ref.getDownloadURL().then((snapshot) => {
      // savePostData(snapshot)
      console.log(snapshot)
      return snapshot
    })
  }

  const taskError = (snapshot) => {
    console.log(snapshot)
  }

  task.on('state_changed', taskProgress, taskError, taskCompleted)
}
