export const fetchApiData = async route => {
  try {
    const response = await fetch(route);
    const json = await response.json();
    return json;
  } catch (error) {
    console.error(error);
    return null;
  }
};