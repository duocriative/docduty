import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import { 
  Home,
  Agenda,
  PacienteConsultas,
  PedirConsultaPaciente,
  Perfil,
} from '../screens/paciente'
import {
  HomeIcon,
  CalendarMonthOutline,
  ProfileIcon,
  HeartPulseIcon,
} from '../assets/icons'

const Tabs = createBottomTabNavigator()
const Stack = createStackNavigator()

const PacienteTabsNavigator = () => {
  return (
    <Tabs.Navigator
      tabBarOptions={{
      activeTintColor: '#43c0ba',
      inactiveTintColor: '#fff',
      style: { backgroundColor: '#000d2f' },
      }}
    >
      <Tabs.Screen
        name="Home"
        component={HomeNavigator}
        options={{
          tabBarIcon: ({ color }) => <HomeIcon fill={color} />,
        }}
      />
      <Tabs.Screen
        name="Agenda"
        component={AgendaNavigator}
        options={{
          tabBarIcon: ({ color }) => <CalendarMonthOutline fill={color} />,
        }}
      />
      <Tabs.Screen
        name="Consultas"
        component={ConsultaNavigator}
        options={{
          tabBarIcon: ({ color }) => <HeartPulseIcon fill={color} />,
        }}
      />
      <Tabs.Screen
        name="Perfil"
        component={PerfilNavigator}
        options={{
          tabBarIcon: ({ color }) => <ProfileIcon fill={color} />,
        }}
      />
    </Tabs.Navigator>
  )
}

export default PacienteTabsNavigator

const HomeNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="PedirConsultaPaciente" component={PedirConsultaPaciente} />
    </Stack.Navigator>
  )
}

const AgendaNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Agenda" component={Agenda} />
    </Stack.Navigator>
  )
}

const ConsultaNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="PacienteConsultas" component={PacienteConsultas} />
    </Stack.Navigator>
  )
}

const PerfilNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Perfil" component={Perfil} />
    </Stack.Navigator>
  )
}