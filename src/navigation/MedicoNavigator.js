import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import {
  Home,
  Plantoes,
  Anuncios,
  Agenda,
  Perfil,
  EditarPerfil,
  AnunciarPlantoes,
  CriarPlantao,
  EditarPlantao,
  DetalhesPlantaoAnuncio,
  DetalhesConsulta,
  DetalhesPlantao,
} from '../screens/medico'

import {
  HomeIcon,
  ProfileIcon,
  HeartPulseIcon,
  CalendarMonthOutline,
  Stethoscope,
} from '../assets/icons'


const Tabs = createBottomTabNavigator()
const Stack = createStackNavigator()

const MedicoTabsNavigator = () => {
  return (
    <Tabs.Navigator
      tabBarOptions={{
        activeTintColor: '#43c0ba',
        inactiveTintColor: '#fff',
        style: { backgroundColor: '#000d2f', paddingTop: 8 },
      }}
      initialRouteName="Home"
    >
      <Tabs.Screen
        name="Agenda"
        component={Agenda}
        options={{
          tabBarIcon: ({ color }) => <CalendarMonthOutline fill={color} />,
        }}
      />
      <Tabs.Screen
        name="Plantoes"
        component={PlantoesNavigator}
        options={{
          tabBarIcon: ({ color }) => <Stethoscope fill={color} />,
        }}
      />
      <Tabs.Screen
        name="Home"
        component={HomeNavigator}
        options={{
          tabBarIcon: ({ color }) => <HomeIcon fill={color} />,
        }}
      />
      <Tabs.Screen
        name="Anúncios"
        component={AnunciosNavigator}
        options={{
          tabBarIcon: ({ color }) => <HeartPulseIcon fill={color} />,
        }}
      />
      <Tabs.Screen
        name="Perfil"
        component={PerfilNavigator}
        options={{
          tabBarIcon: ({ color }) => <ProfileIcon fill={color} />,
        }}
      />
    </Tabs.Navigator>
  )
}

export default MedicoTabsNavigator

const PerfilNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Perfil" component={Perfil} />
      <Stack.Screen name="EditarPerfil" component={EditarPerfil} />
    </Stack.Navigator>
  )
}

const AnunciosNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Anuncios" component={Anuncios} />
      <Stack.Screen name="AnunciarPlantoes" component={AnunciarPlantoes} />
      <Stack.Screen name="CriarPlantao" component={CriarPlantao} />
      <Stack.Screen name="DetalhesPlantaoAnuncio" component={DetalhesPlantaoAnuncio} />
    </Stack.Navigator>
  )
}

const HomeNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="DetalhesPlantao" component={DetalhesPlantao} />
    </Stack.Navigator>
  )
}

const PlantoesNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Plantoes" component={Plantoes} />
      <Stack.Screen name="AnunciarPlantoes" component={AnunciarPlantoes} />
      <Stack.Screen name="CriarPlantao" component={CriarPlantao} />
      <Stack.Screen name="DetalhesPlantaoAnuncio" component={DetalhesPlantaoAnuncio} />
      <Stack.Screen name="EditarPlantao" component={EditarPlantao} />
    </Stack.Navigator>
  )
}
