import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import {
  AuthLoadingScreen,
  LoginScreen,
  RegisterScreen,
  RegisterMedico,
  RegisterPaciente,
  ResetPasswordScreen,
  ProfileScreen,
  HomeScreen,
  ConsultasPacienteScreen,
  AgendaPacienteScreen,
  PlantoesMedicoScreen,
} from '../screens'

const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Messages" component={Messages} />
      <Stack.Screen name="Messages" component={Messages} />
      <Stack.Screen name="Profile" component={Profile} />
    </Stack.Navigator>
  );
};
