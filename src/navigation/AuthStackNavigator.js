import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import {
  AuthLoading,
  Login,
  ResetPassword,
  Register,
  RegisterMedico,
  RegisterPaciente,
} from '../screens/auth'

import MedicoTabsNavigator from './MedicoNavigator'
import PacienteTabsNavigator from './PacienteNavigator'

const Stack = createStackNavigator()

const AuthStackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="AuthLoading"
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="AuthLoading" component={AuthLoading} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="ResetPassword" component={ResetPassword} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="RegisterMedico" component={RegisterMedico} />
      <Stack.Screen name="RegisterPaciente" component={RegisterPaciente} />
      <Stack.Screen name="HomePaciente" component={PacienteTabsNavigator} />
      <Stack.Screen name="HomeMedico" component={MedicoTabsNavigator} />
    </Stack.Navigator>
  )
}

export default AuthStackNavigator
