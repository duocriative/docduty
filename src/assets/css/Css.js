import { StyleSheet } from 'react-native'

const css = StyleSheet.create({
  backGroundImage: {
    flex: 1,
    resizeMode: 'cover',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    width: '80%',
  },
  containerCenter: {
    alignItems: 'center',
  },
})
export { css }
